import { Provider } from "react-redux";
import Store from "./Redux/Store";
import Router from "./Router/Router";
import "./App.css";

const App = () => {
  return (
    <>
      <Provider store={Store}>
        <Router />
      </Provider>
    </>
  );
};

export default App;

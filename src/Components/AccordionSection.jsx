import React from "react";
import { Accordion } from "react-bootstrap";
import OrderItem from "../ProtectedScreens/Cart/CartSections/OrderItem";
import OrderAmount from "./OrderAmount";

const AccordionSection = ({ list, title, type }) => {
  return (
    <Accordion className="mt-3">
      <Accordion.Item eventKey="0">
        <Accordion.Header className="justify-content-between" style={{ backgroundColor: "#f2f2f2" }}>
          <h5 className="me-auto me-2">{title}</h5>
        </Accordion.Header>
        <Accordion.Body>
          {type === "amount" ? (
            <OrderAmount productList={list} />
          ) : (
            list.map((product) => <OrderItem product={product} />)
          )}
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
};

export default AccordionSection;

import { useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

function Footer({ catName }) {
  //If server is down or catName array is empty then default array list of categories will be stored in catName props.
  catName =
    catName?.length > 0
      ? catName
      : ["electronics", "jewelery", "men's clothing", "women's clothing"];

  //function that Scrolls to top smoothly when clicked/called by up-arrow icon.
  const backtotop = () =>
    document
      .getElementById("main-section")
      .scrollTo({ top: 0, behavior: "smooth" });

  useEffect(() => {
    //Logic to hide url from bottom-left-corner of browser screen.
    var aTags = document.querySelectorAll("a[data-href]");
    for (var i = 0; i < aTags.length; i++) {
      var aTag = aTags[i];
      aTag.addEventListener("click", function (e) {
        var ele = e.target;
        window.location.replace(ele.getAttribute("data-href"));
      });
    }
    return () =>
      aTag.removeEventListener("click", () =>
        console.log("Removed Listener!!")
      );
  }, []);

  return (
    <footer
      className="mt-5 position-relative"
      style={{ backgroundColor: "lightgray" }}
    >
      <Container className="p-3 py-5 mt-5">
        <Row xs={1} md={3}>
          <Col>
            <h6 className="fw-bold fs-5">MOST POPULAR CATEGORIES</h6>
            <Row>
              {catName.map((item, index) => (
                <span className="text-capitalize" key={`category_${index}`}>
                  <Link
                    to={`/products`}
                    state={{ catName: item }}
                    className="text-decoration-none fs-5 text-black"
                  >
                    {item}
                  </Link>
                </span>
              ))}
            </Row>
          </Col>
          <Col>
            <h6 className="fw-bold fs-5">CUSTOMER SERVICES</h6>
            <Row>
              {[
                "About us",
                "FAQ",
                "Terms and condition",
                "Privacy policy",
                "Cancelletion and return policy",
              ].map((item, index) => (
                <Link
                  to={`/`}
                  state={{ catName: item }}
                  className="text-decoration-none fs-5 text-black"
                  key={`category_${index}`}
                >
                  <span className="text-capitalize">{item}</span>
                </Link>
              ))}
            </Row>
          </Col>
          <Col>
            <h6 className="fw-bold fs-5">CONTACT US</h6>
            <Row>
              {[
                "Email: shopeemall@gmail.com",
                "Address: Shopmart , Sankalp Iconic tower , 376895 , Ahmedabad , Gujrat.",
                "Phone no: 9876543210",
              ].map((item, index) => (
                <Link
                  to={`/`}
                  className="text-decoration-none fs-5 text-black"
                  key={`category_${index}`}
                >
                  <strong>{item.split(":")[0]}: </strong>
                  <span className="text-capitalize">{item.split(":")[1]}</span>
                </Link>
              ))}
            </Row>
          </Col>
        </Row>
        <Row className="mt-4 align-items-center text-center">
          <hr />
          <Col xs={6} md={11}>
            <strong className="fs-5">
              &#169; Copyrights reserved from 2022
            </strong>
          </Col>
          <Col xs={6} md={1}>
            <a
              className="bi bi-arrow-up-circle-fill fs-1 fw-bold pe-auto"
              data-href="#!"
              style={{ color: "#d2a813", cursor: "pointer" }}
              onClick={backtotop}
            >
              {""}
            </a>
          </Col>
        </Row>
      </Container>
    </footer>
  );
}

export default Footer;

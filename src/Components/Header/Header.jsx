/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Navbar, Container, Form, Nav, NavDropdown } from "react-bootstrap";
import { NavLink, useNavigate } from "react-router-dom";
import { Outlet } from "react-router";
import "./Header.css";
import Footer from "../Footer";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../Redux/Users/UserAction";
import { emptyCart } from "../../Redux/Orders/OrderActions";
import {
  setFilteredProducts,
  setFilter,
  clearFilter,
  getCategories,
} from "../../Redux/Products/ProductActions";

const Header = () => {
  const userInfo = useSelector((state) => state.userList.authUser);
  const products = useSelector((state) => state.cartList.products);
  const wishlist = useSelector((state) => state.wishList.products);
  const filter = useSelector((state) => state.productList.filter);

  //To get array containing list of category name from redux state.
  const categoryList = useSelector((state) => state.categoryList);

  //State for storing boolean value, to show/hide profile menu on hover.
  const [IsAvtarMenu, setIsAvtarMenu] = useState(false);

  const [items, setItems] = useState(0);
  const [text, setText] = useState(filter);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // Set filtered products
  useEffect(() => {
    if (text) {
      dispatch(setFilter(text));
      dispatch(setFilteredProducts(text));
    } else {
      dispatch(clearFilter());
    }
  }, [text]);

  useEffect(() => {
    setText(filter);
  }, [filter]);

  // Set cart items
  useEffect(() => {
    let qty = 0;
    products.map((item) => {
      qty += item.qty;
      return item;
    });
    setItems(qty);
  }, [products]);

  const handleClick = () => {
    dispatch(clearFilter());
    navigate("/");
  };

  useEffect(() => {
    //Redux action(GET_CATEGORIES_SUCCESS) gets trigger by getCategories function.
    dispatch(getCategories());
  }, []);

  useEffect(() => {
    //to show border at bottom of profile till dropdown is open.
    document.getElementById("borderbottom").className = IsAvtarMenu
      ? "highlight d-block"
      : "highlight d-none";
  }, [IsAvtarMenu]);

  const logoutHandler = () => {
    dispatch(logout());
    dispatch(emptyCart());
    // console.log("Logout");
  };

  return (
    <>
      <Navbar
        bg="dark"
        variant="dark"
        className="w-100 p-2 px-4 header"
        expand="md"
      >
        <Container fluid>
          <Navbar.Brand
            className="d-flex align-items-center"
            onClick={handleClick}
          >
            <img
              className="mx-md-3 mx-sm-2"
              src="/logo512.png"
              alt="App Logo"
              width="45"
            />
            <span className="mt-1 ms-2">SHOPEE MALL</span>
          </Navbar.Brand>

          <Form
            className="d-flex justify-content-center mx-4 search-bar"
            onSubmit={(e) => e.preventDefault()}
          >
            <input
              type="search"
              className="w-100 rounded-start px-3 p-2 border-0"
              value={text}
              onChange={(e) => setText(e.target.value)}
              placeholder="Search Products..."
              aria-label="Search"
            />
            <i className="bi bi-search pt-2 text-dark bg-white px-3 rounded-end"></i>
          </Form>

          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse className="flex-grow-0" id="navbarScroll">
            <Nav
              className="ms-auto align-items-center"
              onMouseEnter={() => setIsAvtarMenu(false)}
            >
              {userInfo ? (
                <Nav.Link className="fs-5">
                  <span
                    className="mx-3 d-flex align-items-center"
                    onClick={logoutHandler}
                  >
                    Logout{" "}
                    <i className="fa fa-sign-out p-2" aria-hidden="true" />
                  </span>
                </Nav.Link>
              ) : (
                <div className="fs-5">
                  <NavLink to="login" className="mx-3">
                    Sign In
                  </NavLink>
                  <NavLink to="/signup" className="mx-3">
                    Register
                  </NavLink>
                </div>
              )}

              <NavLink
                to="/wishlist"
                className="d-flex flex-column align-items-center mx-4"
              >
                <div className="position-relative icon-style">
                  {wishlist.length === 0 ? (
                    <i className="bi bi-heart fs-4 icon-style"></i>
                  ) : (
                    <i className="bi bi-heart-fill fs-4 icon-style"></i>
                  )}
                </div>
                <small className="fw-bold">Wishlist</small>
              </NavLink>

              <NavLink
                to="/cart"
                className="d-flex flex-column align-items-center mx-4"
                onMouseEnter={() => setIsAvtarMenu(false)}
              >
                <div className="position-relative icon-style">
                  <span className="cart-number">{items}</span>
                  {!items ? (
                    <i className="bi bi-cart fs-4" aria-hidden="true" />
                  ) : (
                    <i className="bi bi-cart-check fs-4" aria-hidden="true" />
                  )}
                </div>
                <small className="fw-bold">Cart</small>
              </NavLink>

              <NavDropdown
                title={
                  <div className="position-relative">
                    <span
                      className="d-flex flex-column align-items-center"
                      id="profile-btn"
                      onClick={() => navigate("/profile")}
                    >
                      <i
                        className="bi bi-person fs-4 icon-style"
                        aria-hidden="true"
                      />
                      <small className="fw-bold">Profile</small>
                    </span>
                    <div className="highlight d-none" id="borderbottom"></div>
                  </div>
                }
                className="position-relative"
                show={IsAvtarMenu}
                onMouseEnter={() => setIsAvtarMenu(true)}
              >
                <NavDropdown.ItemText as="li" className="py-1">
                  <NavLink to="/profile">Profile</NavLink>
                </NavDropdown.ItemText>
                <NavDropdown.ItemText as="li" className="py-2">
                  <NavLink to="/orders">Orders</NavLink>
                </NavDropdown.ItemText>
                <NavDropdown.ItemText as="li" className="py-1">
                  <NavLink to="/wishlist">Wishlist</NavLink>
                </NavDropdown.ItemText>
                {userInfo && (
                  <>
                    <NavDropdown.Divider />
                    <NavDropdown.ItemText as="li" onClick={logoutHandler}>
                      <NavLink to="/orders">Logout</NavLink>
                    </NavDropdown.ItemText>
                  </>
                )}
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <main id="main-section" onMouseEnter={() => setIsAvtarMenu(false)}>
        {
          //To scroll to top whenever this component is called.
          document
            .getElementById("main-section")
            ?.scroll({ top: 0, behavior: "smooth" })
        }
        <Outlet />
        <Footer catName={categoryList.categories} />
      </main>
    </>
  );
};

export default Header;

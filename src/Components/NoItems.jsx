import React from "react";
import Message from "./Message";
import emptySvg from "../Assets/Images/emptyCart.svg";

const NoItems = ({ title }) => {
  return (
    <Message variant="secondary">
      <div className="text-center">
        <h4 className="mb-4"> {title} </h4>
        <img src={emptySvg} style={{ height: "200px" }} alt="empty cart" />
      </div>
    </Message>
  );
};

export default NoItems;

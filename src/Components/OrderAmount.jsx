import React, { useEffect, useState } from "react";
import { Card, Col, ListGroup, Row } from "react-bootstrap";
import { useSelector } from "react-redux";

const OrderAmount = ({ productList }) => {
  const products = useSelector((state) => state.cartList.products);
  const list = productList || products;
  const [price, setPrice] = useState(0);
  const [items, setItems] = useState(0);
  const [deliveryCharges, setDeliveryCharges] = useState();

  useEffect(() => {
    let price = 0;
    let qty = 0;
    list.map((item) => {
      price += item.price * item.qty;
      qty += item.qty;
      return item;
    });
    setPrice(price);
    setItems(qty);
    setDeliveryCharges(price === 0 ? 0 : 50);
  }, [list]);

  return (
    <Card className="order-amount mt-3">
      <Card.Header className="text-center" as="h5">
        Order Amount Details
      </Card.Header>
      <ListGroup>
        <ListGroup.Item>
          <h6 className="text-start text-uppercase fw-bold">
            Price Details
            <span className="text-secondary">{`(Items: ${items})`}</span>{" "}
          </h6>
        </ListGroup.Item>
        <ListGroup.Item>
          {/*PRICE ROW */}
          <Row>
            <Col md={6} className="text-start">
              <p>Price</p>
            </Col>
            <Col md={6} className="text-end">
              <p>&#8377; {Math.round(price * 76)}</p>
            </Col>
          </Row>
        </ListGroup.Item>
        <ListGroup.Item>
          {/*DELIVERY CHARGES */}
          <Row>
            <Col md={6} className="text-start">
              <p>Delivery Charges</p>
            </Col>
            <Col md={6} className="text-end">
              <p>&#8377; {Math.round(price * 76)>1000 ? 0 : deliveryCharges}</p>
            </Col>
          </Row>
        </ListGroup.Item>
        
      </ListGroup>

      {/*TOTAL AMOUNT */}
      <Card.Footer className="text-success">
        <Row>
          <Col md={6}>
            <p className="text-start fw-bold">Total Amount</p>
          </Col>
          <Col md={6} className="text-end">
            <p className="fw-bold">
              &#8377; {Math.round(price * 76)>1000 ? Math.round(price * 76):Math.round(price * 76)+ deliveryCharges}
            </p>
          </Col>
        </Row>
      </Card.Footer>
    </Card>
  );
};

export default OrderAmount;

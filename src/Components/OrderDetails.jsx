/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Card, Col, Container, ListGroup, Row } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import Loading from "./Loading";
import { getProductDetails } from "../Redux/Products/ProductActions";
import Message from "./Message";
import AccordionSection from "./AccordionSection";

const OrderDetails = ({ order }) => {
  const [productList, setProductList] = useState(order?.products);

  const deliveryDate = new Date(
    Date.now() + 3600 * 1000 * 72
  ).toLocaleDateString("en-us", {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric",
  });

  const productDetails = useSelector((state) => state.productDetails);
  const { loading, product, error } = productDetails;

  const dispatch = useDispatch();

  useEffect(() => {
    if (order) {
      order.products.map((product) =>
        dispatch(getProductDetails(product.productId))
      );
    }
  }, [order]);

  useEffect(() => {
    if (product) {
      const arr = productList.map((item) => {
        if (item.productId === product.id) {
          product.qty = item.quantity;
          return product;
        } else {
          return item;
        }
      });
      setProductList(arr);
    }
  }, [product]);

  return loading ? (
    <Loading />
  ) : !order ? (
    <Message variant="danger">
      <i className="fa fa-warning text-danger" />
      {"  "} Something is wrong. Please try again!
    </Message>
  ) : error ? (
    <Message variant="danger">
      <i className="fa fa-warning text-danger" />
      {"  "}
      {error}
    </Message>
  ) : (
    <Card className='rounded-0 rounded-bottom '>
      <Card.Body>
        <Container>
          <h5 className="fw-bold">Order ID: #{order.id}</h5>
          <small className="text-muted ">
            Ordered on: {order.date.slice(0, 10)}
          </small>
          <ListGroup>
            {productList.map((product, index) => (
              <ListGroup.Item key={index}>
                <Row className="p-3">
                  <Col md={3}>
                    <img
                      src={product.image}
                      style={{ height: "120px", width: "120px" }}
                      alt={product.title}
                    />
                  </Col>

                  <Col md={6} className="fs-6">
                    <p style={{ fontSize: "15px" }}>{product.title}</p>
                    <p style={{ fontSize: "15px", color: "gray" }}>
                      &#8377; {Math.round(product.price * 76)} x {product.qty} ={" "}
                      &#8377; {product.price * product.qty * 76}
                    </p>
                    <p
                      style={{
                        fontSize: "15px",
                        color: "gray",
                        textTransform: "capitalize",
                      }}
                    >
                      {product.category}
                    </p>
                  </Col>

                  <Col md={3}>
                    {product.isDelivered ? (
                      <>
                        {" "}
                        <i
                          className="fa-solid fa-circle text-success"
                          style={{ fontSize: "7px" }}
                        />
                        <span className="ms-2" style={{ fontSize: "13px" }}>
                          Deliverd on Oct 18,2021
                        </span>
                        <p style={{ fontSize: "13px" }} className="mt-1">
                          Your product has been delivered
                        </p>
                      </>
                    ) : (
                      <p style={{ fontSize: "13px" }}>
                        <i
                          className="fa fa-check text-success "
                          aria-hidden="true"
                        />{" "}
                        Delivery by <i className="fw-bold">{deliveryDate}</i>
                      </p>
                    )}
                  </Col>
                </Row>
              </ListGroup.Item>
            ))}
            <ListGroup.Item>
              <AccordionSection
                list={productList}
                type="amount"
                title="View Order Details"
              />
            </ListGroup.Item>
          </ListGroup>
        </Container>
      </Card.Body>
    </Card>
  );
};

export default OrderDetails;

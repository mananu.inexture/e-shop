import React from "react";
import { Link } from "react-router-dom";
import { Button, Card } from "react-bootstrap";
import "./ProductCard.css";
import Rating from "./Rating";

const ProductCard = ({ product }) => {
  return (
    <Card className="border-0 p-4 product-card" key={product.id}>
      <Link
        to={`/product/${product.id}`}
        className="text-decoration-none text-dark text-center"
      >
        <Card.Img
          className="align-self-center mb-3 img-fluid"
          variant="top"
          style={{ height: "20vh", width: "auto" }}
          src={product.image}
        />
        <Card.Body>
          <Card.Title className="fs-5 fw-bold text-truncate mb-3">
            {product.title}
          </Card.Title>
          <Card.Text className="fw-bolder mb-3">
            &#8377;{Math.round(product.price * 76)}
          </Card.Text>
          <Rating value={product.rating?.rate} />
        </Card.Body>
        <Button variant="outline-dark m-4">View Details</Button>
      </Link>
    </Card>
  );
};

export default ProductCard;

import { useEffect, useState } from "react";

const useGeoLocationHook = () => {
  const [sysLocation, setSysLocation] = useState({
    loaded: false,
    coordinates: {
      lat: "",
      lng: "",
    },
  });
  const onSuccess = (location) => {
    setSysLocation({
      loaded: true,
      coordinates: {
        lat: location.coords.latitude,
        lng: location.coords.longitude,
      },
    });
  };
  const onError = (error) =>
    setSysLocation({ loaded: false, error: error.message });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (!("geolocation" in navigator)) {
      setSysLocation((state) => ({
        ...state,
        loaded: true,
        error: {
          message: "Geolocation is not supported",
        },
      }));
    }
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
  }, []);

  return sysLocation;
};

export default useGeoLocationHook;

import React, { useEffect } from "react";
import { Accordion, Col, Container, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useOutletContext } from "react-router-dom";
import Loading from "../../../Components/Loading";
import Message from "../../../Components/Message";
import OrderDetails from "../../../Components/OrderDetails";
import { getAllOrders } from "../../../Redux/Orders/OrderActions";
import {
  getCategories,
  getProducts,
} from "../../../Redux/Products/ProductActions";
import { getUsers } from "../../../Redux/Users/UserAction";
import "../Components/Admin.css";

const AdminDashboard = () => {
  const userList = useSelector((state) => state.userList);
  const { loading: userLoading, error: userError, users } = userList;

  const productList = useSelector((state) => state.productList);
  const {
    loading: productLoading,
    error: productError,
    products,
  } = productList;

  const categoryList = useSelector((state) => state.categoryList);
  const {
    loading: categoryLoading,
    error: categoryError,
    categories,
  } = categoryList;

  const orderList = useSelector((state) => state.orderList);
  const { loading: orderLoading, error: orderError, orders } = orderList;

  // console.log(orderList);
  const dispatch = useDispatch();
  const outlet = useOutletContext();

  useEffect(() => {
    outlet.selectCurTab("/admin/dashboard");
    dispatch(getCategories());
    dispatch(getProducts());
    dispatch(getAllOrders({}));
    dispatch(getUsers());
    // eslint-disable-next-line
  }, []);

  return (
    <Container>
      {(userError || productError || categoryError || orderError) && (
        <Message variant="danger">
          <i className="fa fa-warning text-danger" />
          {"  "}
          {userError || productError || categoryError || orderError}
        </Message>
      )}
      <h1>Dashboard</h1>
      <Row className="my-3">
        <Col md={3}>
          <div className="p-2 bg-white text-dark">
            {productLoading ? (
              <Loading size={50} />
            ) : (
              <>
                <div className="inner">
                  <h3>{products.length}</h3>
                  <p>Total Products</p>
                </div>
                <Link to="/admin/products" className="g-color">
                  More info <i className="fas fa-arrow-circle-right"></i>
                </Link>
              </>
            )}
          </div>
        </Col>

        <Col md={3}>
          <div className="p-2 bg-white text-dark">
            {categoryLoading ? (
              <Loading size={50} />
            ) : (
              <>
                <div className="inner">
                  <h3>{categories.length}</h3>
                  <p>Total Categories</p>
                </div>
                <Link to="/admin/categories" className="g-color">
                  More info <i className="fas fa-arrow-circle-right"></i>
                </Link>
              </>
            )}
          </div>
        </Col>

        <Col md={3}>
          <div className="p-2 bg-white text-dark">
            {userLoading ? (
              <Loading size={50} />
            ) : (
              <>
                <div className="inner">
                  <h3>{users.length}</h3>
                  <p>Total Users</p>
                </div>
                <Link to="/admin/users" className="g-color">
                  More info <i className="fas fa-arrow-circle-right"></i>
                </Link>
              </>
            )}
          </div>
        </Col>

        <Col md={3}>
          <div className="p-2 bg-white text-dark">
            {orderLoading ? (
              <Loading size={50} />
            ) : (
              <>
                <div className="inner">
                  <h3>{orders.length}</h3>
                  <p>Total Orders</p>
                </div>
                <Link to="/admin/orders" className="g-color">
                  More info <i className="fas fa-arrow-circle-right"></i>
                </Link>
              </>
            )}
          </div>
        </Col>
      </Row>
      <hr />
      <Row>
        <h3 className="m-0">Latest Orders:</h3>
        {orders
          .sort((a, b) => {
            return new Date(b.date) - new Date(a.date);
          })
          .slice(0, 5)
          .map((order) => (
            <Container>
              <Accordion className="accordian-custom">
                <Accordion.Header className="d-flex bg-dark justify-content-between text-light fw-bold mt-4">
                  <h5>User ID: {order.userId}</h5>
                  <h5 className="ms-auto me-2">
                    {"  "}Username:{" "}
                    {users.find((user) => user.id === order.userId)?.username}
                  </h5>
                  {/* <h5 className="text-end">
                      Email: {users.find((user) => user.id === order.userId)?.email}
                    </h5> */}
                </Accordion.Header>
                <Accordion.Body className="accordian-custom-body">
                  <OrderDetails order={order} />
                </Accordion.Body>
              </Accordion>
            </Container>
          ))}
      </Row>
    </Container>
  );
};

export default AdminDashboard;

import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import {
  Button,
  Container,
  Form,
  Modal,
  Row,
  Col,
  FloatingLabel,
} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  addProduct,
  updateProduct,
} from "../../../Redux/Products/ProductActions";
import { yupResolver } from "@hookform/resolvers/yup";
import { productSchema } from "../../../Validations/index";

const AddProduct = ({ Btn, edit = false, product = null }) => {
  // console.log(product);
  const [show, setShow] = useState(false);
  const [image, setImage] = useState(product?.image);

  useEffect(() => {
    reset();
    // eslint-disable-next-line
  }, []);

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(productSchema),
    defaultValues: {
      title: edit ? product.title : "",
      price: edit ? product.price : "",
      category: edit ? product.category : "",
      description: edit ? product.description : "",
      image: edit ? product.image : "",
    },
  });

  const handleClose = () => {
    reset();
    setShow(false);
  };
  const handleShow = () => setShow(true);

  const dispatch = useDispatch();

  const categories = useSelector((state) => state.categoryList.categories);

  const onSubmit = (data) => {
    if (edit) {
      dispatch(updateProduct(product.id, data));
    } else {
      dispatch(addProduct(data));
    }
    reset();
    handleClose();
  };

  return (
    <>
      <span onClick={handleShow}>{Btn}</span>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>{edit ? "Edit Product" : "Add Product"}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Container>
              <div
                className="d-flex justify-content-center my-3"
                style={{ minHeight: "100px" }}
              >
                <img
                  src={
                    image ||
                    "https://img.icons8.com/clouds/100/000000/used-product.png"
                  }
                  alt="product"
                  width="120px"
                  onError={(e) =>
                    (e.target.src =
                      "https://img.icons8.com/clouds/100/000000/used-product.png")
                  }
                />
              </div>
              <Row className="mt-2 gx-3">
                {/* Title field */}
                <Col md={12}>
                  <FloatingLabel label="Title">
                    <Form.Control
                      {...register("title")}
                      type="text"
                      placeholder="title"
                    />
                  </FloatingLabel>
                  {errors.title && (
                    <small className="text-danger">
                      {errors.title.message}
                    </small>
                  )}
                </Col>
              </Row>

              <Row className="mt-2 gx-3">
                {/* Price field */}
                <Col md={6}>
                  <FloatingLabel label="Price">
                    <Form.Control
                      {...register("price")}
                      type="text"
                      placeholder="Price"
                    />
                  </FloatingLabel>
                  {errors.price && (
                    <small className="text-danger">
                      {errors.price.message}
                    </small>
                  )}
                </Col>

                {/* Category field */}
                <Col md={6}>
                  <FloatingLabel label="Category">
                    <Form.Select {...register("category")}>
                      <option>Select Category</option>
                      {categories.map((category, index) => (
                        <option value={category} key={index}>
                          {category[0].toUpperCase()}
                          {category.slice(1)}
                        </option>
                      ))}
                    </Form.Select>
                  </FloatingLabel>
                  <span className="text-danger">
                    {errors.category && errors.category.message}
                  </span>
                </Col>
              </Row>

              <Row className="mt-2 gx-3">
                {/* Image field */}
                <Col md={12}>
                  <FloatingLabel label="Image Url">
                    <Form.Control
                      {...register("image")}
                      type="text"
                      placeholder="Image Url"
                      onChange={(e) => setImage(e.target.value)}
                    />
                  </FloatingLabel>
                  {errors.image && (
                    <small className="text-danger">
                      {errors.image.message}
                    </small>
                  )}
                </Col>
              </Row>

              <Row className="mt-2 gx-3">
                {/* Description field */}
                <Col md={12}>
                  <FloatingLabel label="Description">
                    <Form.Control
                      {...register("description")}
                      as="textarea"
                      placeholder="Description"
                      style={{ height: "100px" }}
                    />
                  </FloatingLabel>
                  {errors.description && (
                    <small className="text-danger">
                      {errors.description.message}
                    </small>
                  )}
                </Col>
              </Row>
            </Container>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" type="submit">
              {edit ? "Edit" : "Add"}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default AddProduct;

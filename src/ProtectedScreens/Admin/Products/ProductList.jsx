import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Container, Row, Col, Button } from "react-bootstrap";
import { useOutletContext } from "react-router-dom";
import Pagination from "../Components/Pagination";
import {
  deleteProduct,
  getCategories,
  getProducts,
} from "../../../Redux/Products/ProductActions";
import Loading from "../../../Components/Loading";
import Message from "../../../Components/Message";
import AddProduct from "./AddProduct";

const ProductList = () => {
  const productList = useSelector((state) => state.productList);
  const { products, loading, error } = productList;
  const [filterData, setFilterData] = useState([]);

  const dispatch = useDispatch();
  const outlet = useOutletContext()
  useEffect(() => {
    outlet.selectCurTab('/admin/products');
    // props.selectCurrTab('/admin/products');
    dispatch(getCategories());
    if (products.length === 0) {
      dispatch(getProducts());
    }
    // eslint-disable-next-line
  }, []);

  const deleteHandler = (id, category) => {
    if (window.confirm("Are you sure? This is an IRREVERSIBLE action!")) {
      dispatch(deleteProduct(id, category));
    }
  };
  const onChangePage = (pageOfItems) => {
    setFilterData(pageOfItems);
  };
  return (
    <Container>
      <Row className="m-2">
        <Col>
          <h3>Products Data</h3>
        </Col>
        <Col className="text-end">
          <AddProduct
            Btn={
              <Button variant="primary">
                <i className="bi bi-plus-square m-1"></i> Add Product
              </Button>
            }
          />
        </Col>
      </Row>
      <Row>
        {loading ? (
          <Loading />
        ) : error ? (
          <Message variant="danger">
            <i className="fa fa-warning text-danger" />
            {"  "}
            {error}
          </Message>
        ) : products.length === 0 ? (
          <Message>Sorry, No Data Found!</Message>
        ) : (
          <>
            <Table bordered hover responsive className="disp-product bg-white">
              <thead>
                <tr>
                  <th>Sr No.</th>
                  <th>ID</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Price</th>
                  {/* <th>Description</th> */}
                  <th>Category</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {loading && "Loading..."}
                {filterData.map((product, index) => (
                  <tr className="align-middle" key={index}>
                    <td width='4%'>{index + 1}</td>
                    <td width="4%">{product?.id}</td>
                    <td width="10%" className=" text-center">
                      <img
                        className="w-50 border border-1"
                        src={product?.image}
                        alt="product logo"
                      />{" "}
                    </td>
                    <td width="20%">{product?.title}</td>
                    <td width="5%">{product?.price}</td>
                    <td width="10%" className="text-capitalize">
                      {product?.category}
                    </td>
                    <td width="10%">
                      <div className="d-flex justify-content-evenly align-items-center actions">
                        <AddProduct
                          Btn={<i className="bi bi-pencil-square fs-5"></i>}
                          edit={true}
                          product={product}
                        />
                        <i
                          className="bi bi-trash text-danger fs-5"
                          onClick={() =>
                            deleteHandler(product.id, product.category)
                          }
                        ></i>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <div className="text-center d-flex justify-content-center">
              <Pagination
                pageSize={10}
                items={products}
                onChangePage={onChangePage}
              />
            </div>
          </>
        )}
      </Row>
    </Container>
  );
};

export default ProductList;

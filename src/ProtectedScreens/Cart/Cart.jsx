import React, { useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import Message from "../../Components/Message";
import NoItems from "../../Components/NoItems";
import OrderAmount from "../../Components/OrderAmount";
import OrderItem from "./CartSections/OrderItem";

const Cart = () => {
  const [message, setMessage] = useState("");
  const products = useSelector((state) => state.cartList.products);

  const navigate = useNavigate();

  const handleCheckout = () => {
    if (products.length === 0) {
      setMessage("Please add some products in your cart!");
      setTimeout(() => {
        setMessage("");
      }, 4000);
    } else {
      navigate("/checkout");
    }
  };

  return (
    <Container>
      {message && (
        <Message variant="danger">
          {"  "}
          {message}
        </Message>
      )}
      <Card className="my-2 p-3 text-center">
        <h4 className="fw-bold">YOUR BAG</h4>
        <Container>
          {products.length === 0 ? (
            <>
              {" "}
              <NoItems title="Your Cart is Currently Empty!" />
              <Link to="/">
                <Button variant="dark">Shop now</Button>
              </Link>
            </>
          ) : (
            <Row className="text-end">
              {/*ORDER ITEMS SECTION*/}
              <Col md={8}>
                {products.map((product, index) => (
                  <OrderItem product={product} key={`orderItem_${index}`} />
                ))}
              </Col>
              {/*ORDER AMOUNT SECTION */}
              <Col md={4}>
                <OrderAmount />
              </Col>
              <div className="mt-3">
                <Button variant="dark" onClick={handleCheckout}>
                  Proceed To Checkout
                </Button>
              </div>
            </Row>
          )}
        </Container>
      </Card>
    </Container>
  );
};

export default Cart;

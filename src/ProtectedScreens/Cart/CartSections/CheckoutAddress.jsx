/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Card, Col, Form, ListGroup, Row } from "react-bootstrap";
import { useSelector } from "react-redux";

const CheckoutAddress = ({ changeAddress }) => {
  const user = useSelector((state) => state.userList.authUser);
  // console.log(user);
  // console.log("USER", user.address);

  const [address, setAddress] = useState(null);
  const [display, setDisplay] = useState({
    existing: false,
    new: false,
  });
  const [formData, setFormData] = useState({
    city: "",
    street: "",
    number: "",
    zipcode: "",
    phone: "",
  });

  useEffect(() => {
    changeAddress(address);
  }, [address]);

  useEffect(() => {
    if (display.new) {
      setAddress(formData);
    } else if (display.existing) {
      setAddress(user.address);
    } else {
      setAddress(null);
    }
  }, [display]);
  //${user.address.number},${user.address.zipcode}
  return (
    <div>
      <Card className="checkout-address mt-3">
        <Card.Header as="h5">Delivery Address</Card.Header>
        <ListGroup>
          <ListGroup.Item>
            {" "}
            <Form.Check
              inline
              label={`${user.address.street}, ${user.address.city} `}
              name="group1"
              onChange={(e) => {
                if (e.target.checked) {
                  setDisplay({ existing: true, new: false });
                  setAddress(user.address);
                } else {
                  setDisplay({ existing: false, new: false });
                  setAddress(null);
                }
              }}
              checked={display.existing}
              disabled={display.new && !display.existing}
            />
          </ListGroup.Item>
          {display && (
            <ListGroup.Item>
              <Form.Check
                inline
                label="Add New Address :"
                name="group1"
                onChange={(e) => {
                  if (e.target.checked) {
                    setDisplay({ existing: false, new: true });
                  } else {
                    setDisplay({ existing: false, new: false });
                  }
                }}
                checked={display.new}
                disabled={!display.new && display.existing}
              />
              <Row>
                <Col md={6}>
                  <input
                    className="form-control mt-2"
                    name="number"
                    type="text"
                    value={formData.number}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        [e.target.name]: e.target.value,
                      })
                    }
                    onBlur={() => setAddress(formData)}
                    placeholder="Block Number"
                    required
                    disabled={!display.new && display.existing}
                  />
                </Col>
                <div className="col-md-6">
                  <input
                    className="form-control mt-2"
                    name="street"
                    type="text"
                    value={formData.street}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        [e.target.name]: e.target.value,
                      })
                    }
                    onBlur={() => setAddress(formData)}
                    placeholder="Street"
                    required
                    disabled={!display.new && display.existing}
                  />
                </div>
                <div className="col-md-6">
                  <input
                    className="form-control mt-2"
                    name="city"
                    type="text"
                    value={formData.city}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        [e.target.name]: e.target.value,
                      })
                    }
                    onBlur={() => setAddress(formData)}
                    placeholder="City"
                    required
                    disabled={!display.new && display.existing}
                  />
                </div>

                <div className="col-md-6">
                  <input
                    className="form-control mt-2"
                    name="zipcode"
                    type="text"
                    value={formData.zipcode}
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        [e.target.name]: e.target.value,
                      })
                    }
                    onBlur={() => setAddress(formData)}
                    placeholder="ZIP Code"
                    required
                    disabled={!display.new && display.existing}
                  />
                </div>
                <div className="col-md-6">
                  <input
                    className="form-control mt-2"
                    type="text"
                    name="phone"
                    onChange={(e) =>
                      setFormData({
                        ...formData,
                        [e.target.name]: e.target.value,
                      })
                    }
                    onBlur={() => setAddress(formData)}
                    value={formData.mobile_number}
                    placeholder="Mobile Number"
                    required
                    disabled={!display.new && display.existing}
                  />
                </div>
              </Row>
            </ListGroup.Item>
          )}
        </ListGroup>
      </Card>
    </div>
  );
};
export default CheckoutAddress;

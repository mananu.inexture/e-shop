import React from "react";
import { useSelector } from "react-redux";
import { Card, Col, Container, Row } from "react-bootstrap";

function DeliveryDetails({ address, payment }) {
  const user = useSelector((state) => state.userList.authUser);

  return (
    <Card className="mt-4">
      <Container>
        <Row>
          <Col md={6} className="mt-3">
            <h4>Delivery Address</h4>
            <h6 className="text-capitalize">
              {user.name.firstname} {user.name.lastname}{" "}
            </h6>
            <p>
              {address.number}, {address.street}, {address.city} -{" "}
              {address.zipcode}
            </p>
            <h6>Contact Number</h6>
            <p>{address.phone ? address.phone : user.phone}</p>
          </Col>
          <Col md={6} className="mt-3">
            <h4>Payment Method</h4>
            <h6 className="text-capitalize">{payment}</h6>
          </Col>
        </Row>
      </Container>
    </Card>
  );
}

export default DeliveryDetails;

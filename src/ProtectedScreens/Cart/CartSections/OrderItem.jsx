import React, { useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";
import {
  deleteFromCart,
  setQuantity,
} from "../../../Redux/Orders/OrderActions";

const OrderItem = ({ product }) => {
  const [qty, setQty] = useState(product.qty);

  const deliveryDate = new Date(
    Date.now() + 3600 * 1000 * 72
  ).toLocaleDateString("en-us", {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric",
  });

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setQuantity(product.id, qty));
    // eslint-disable-next-line
  }, [qty]);
  if (!product) return "Your Cart is Empty!";

  /* ORDER ITEMS SECTION */
  return (
    <Card className="mt-3">
      <div className="text-end">
        {/*CLOSE BUTTON */}
        <Button
          variant="close"
          onClick={() => dispatch(deleteFromCart(product.id))}
          className="p-3"
          aria-label="Close"
        />
      </div>
      <Container>
        <Row>
          <Col
            md={5}
            className="d-flex align-items-center justify-content-center"
          >
            <img
              src={product.image}
              style={{ height: "150px", width: "150px" }}
              // className="img-thumbnail"
              alt="..."
            />
          </Col>
          <Col md={7}>
            <Card.Body>
              <Card.Title>
                <h5>{product.title}</h5>
              </Card.Title>
              {/* <p>{`Product ID: ${cartItems[0].id}`}</p> */}
              <h4 className="text-success">
                &#8377; {Math.round(product.price * 76)}
              </h4>
              <Row className="mt-3">
                <Col md={8}>
                  <p className="mt-2 fw-bold">Select Quantity</p>
                </Col>
                <Col
                  md={4}
                  className="text-end d-flex justify-content-end align-items-center "
                >
                  <Button
                    variant="secondary"
                    className="h-75"
                    onClick={() => {
                      setQty(qty > 1 ? qty - 1 : 1);
                    }}
                    disabled={qty === 1}
                  >
                    <i
                      className="fa fa-minus fa-2xs"
                      style={{ fontSize: "10px" }}
                      aria-hidden="true"
                    />
                  </Button>
                  <span className="mx-3">{qty}</span>
                  <Button
                    variant="secondary"
                    className="h-75"
                    onClick={() => {
                      setQty(qty < 10 ? qty + 1 : 10);
                    }}
                    disabled={qty === 10}
                  >
                    <i
                      className="fa fa-plus fa-2xs"
                      style={{ fontSize: "10px" }}
                      aria-hidden="true"
                    />
                  </Button>
                </Col>
              </Row>
              <p className="mt-4">
                <i className="fa fa-check text-success " aria-hidden="true" />{" "}
                Delivery by <i className="fw-bold">{deliveryDate}</i>
              </p>
            </Card.Body>
          </Col>
        </Row>
      </Container>
    </Card>
  );
};
export default OrderItem;

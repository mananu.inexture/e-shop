/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Card, Col, Form, Row } from "react-bootstrap";

function PaymentOptions({ changePayment }) {
  const [option, setOption] = useState("");

  useEffect(() => {
    changePayment(option);
  }, [option]);

  return (
    <Card className="mt-3">
      <Card.Header as="h5">Payment Options</Card.Header>
      <Row className="p-3 text-center">
        <Col md={3}>
          <Form.Check
            inline
            type="radio"
            label="Debit Card"
            name="payment_option"
            value="Debit Card"
            onChange={(e) => {
              if (e.target.checked) {
                setOption(e.target.value);
              }
            }}
          />
        </Col>
        <Col md={3}>
          <Form.Check
            inline
            type="radio"
            label="Net Banking"
            name="payment_option"
            value="Net Banking"
            onChange={(e) => {
              if (e.target.checked) {
                setOption(e.target.value);
              }
            }}
          />
        </Col>
        <Col md={3}>
          <Form.Check
            inline
            type="radio"
            label="UPI"
            name="payment_option"
            value="UPI"
            onChange={(e) => {
              if (e.target.checked) {
                setOption(e.target.value);
              }
            }}
          />
        </Col>
        <Col md={3}>
          <Form.Check
            inline
            type="radio"
            label="Pay on Delivery"
            name="payment_option"
            value="Pay on Delivery"
            onChange={(e) => {
              if (e.target.checked) {
                setOption(e.target.value);
              }
            }}
          />
        </Col>
      </Row>
    </Card>
  );
}

export default PaymentOptions;

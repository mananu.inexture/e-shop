import React, { useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import Message from "../../Components/Message";
import {
  addOrder,
  emptyCart,
  setAddress,
} from "../../Redux/Orders/OrderActions";
import CheckoutAddress from "./CartSections/CheckoutAddress";
import OrderAmount from "../../Components/OrderAmount";
import PaymentOptions from "./CartSections/PaymentOptions";
import AccordionSection from "../../Components/AccordionSection";
import NoItems from "../../Components/NoItems";

const ProceedCheckout = () => {
  const [addr, setAddr] = useState(null);
  const [payment, setPayment] = useState(null);
  const [message, setMessage] = useState(null);

  const cart = useSelector((state) => state.cartList);

  const changeAddress = (ad) => {
    setAddr(ad);
  };
  const changePayment = (pay) => {
    setPayment(pay);
  };
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleClick = () => {
    if (!addr) {
      setMessage("Please add a delivery address!");
      window.scrollTo({ top: 0, behavior: "smooth" });
      setTimeout(() => {
        setMessage(null);
      }, 4000);
      return;
    }
    if (!payment) {
      setMessage("Please add a payment method!");
      window.scrollTo({ top: 0, behavior: "smooth" });
      setTimeout(() => {
        setMessage(null);
      }, 4000);
      return;
    }

    dispatch(setAddress(addr));
    dispatch(addOrder(cart));
    navigate("/shipped", { state: { payment, address: addr } });
    dispatch(emptyCart());
  };
  return (
    <Container className="checkout">
      {message && (
        <Message variant="danger">
          <i className="fa fa-warning text-danger" />
          {"  "}
          {message}
        </Message>
      )}
      <Card className="my-3 p-3">
        <h2 className="text-center">Order Checkout</h2>
        <Container>
          {cart.products.length === 0 ? (
            <NoItems title="Your Cart is Currently Empty!" />
          ) : (
            <>
              <Row>
                <Col md={8}>
                  <CheckoutAddress changeAddress={changeAddress} />
                </Col>
                <Col md={4}>
                  <OrderAmount />
                </Col>
              </Row>
              <PaymentOptions changePayment={changePayment} />
              <AccordionSection
                list={cart.products}
                type="orders"
                title="View Order Items"
              />
            </>
          )}
        </Container>
      </Card>
      {cart.products.length !== 0 && (
        <div className="text-center my-4">
          <Button
            variant="dark"
            style={{ width: "150px" }}
            onClick={handleClick}
          >
            Place Order
          </Button>
        </div>
      )}
    </Container>
  );
};
export default ProceedCheckout;

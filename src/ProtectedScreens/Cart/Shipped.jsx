import React, { useEffect } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import gift from "../../Assets/Images/gift.svg";
import OrderDetails from "../../Components/OrderDetails";
import DeliveryDetails from "./CartSections/DeliveryDetails";
import { useLocation, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import Loading from "../../Components/Loading";
import Message from "../../Components/Message";

function Shipped() {
  const { state } = useLocation();

  const orderList = useSelector((state) => state.orderList);
  const { loading, currentOrder, error } = orderList;
  // console.log(state);
  const navigate = useNavigate();
  useEffect(() => {
    if (!state) navigate("/");
    // eslint-disable-next-line
  }, [state]);

  return loading || !state || !currentOrder ? (
    <Loading />
  ) : error ? (
    <Message variant="danger">
      <i className="fa fa-warning text-danger" />
      {"  "}
      {error}
    </Message>
  ) : (
    <Container>
      <Card className="card">
        <Row>
          <Col md={2}>
            <img src={gift} style={{ height: "150px" }} alt="gift icon" />
          </Col>
          <Col
            md={8}
            className="d-flex align-items-center justify-content-center"
          >
            <div className="text-primary text-center">
              <h2>Thank You</h2>
              <h3>Your Order is Placed!!</h3>
            </div>
          </Col>

          {/* <Col md={4}>
            <div className="text-secondary" style={{ marginTop: "20px" }}>
              <h4>Just Click, Here.</h4>
              <h5>To Checkout Order Details!!</h5>
            </div>
            <Button variant="info">Order Details</Button>
          </Col> */}
        </Row>
      </Card>
      <DeliveryDetails address={state.address} payment={state.payment} />
      <OrderDetails order={currentOrder} />
    </Container>
  );
}
export default Shipped;

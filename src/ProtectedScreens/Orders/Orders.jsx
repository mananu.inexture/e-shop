import React, { useEffect, useState } from "react";
import { Col, Container, Form, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import Loading from "../../Components/Loading";
import { getAllOrders, getUserOrders } from "../../Redux/Orders/OrderActions";
import OrderDetails from "../../Components/OrderDetails";
import Message from "../../Components/Message";

const Orders = ({ allOrders = false }) => {
  const [dateFilter, setDateFilter] = useState("all");
  const [statusFilter, setStatusFilter] = useState([]);
  const [filteredOrders, setFilteredOrders] = useState(null);

  const statusArr = ["On the way", "Delivered", "Cancelled", "Returned"];
  const dateRangeArr = ["All", "Last 30 days", "2020", "2019", "older"];

  const user = useSelector((state) => state.userList.authUser);

  const orderList = useSelector((state) => state.orderList);
  const { loading, orders, error } = orderList;

  const dispatch = useDispatch();

  const getOrders = (startdate = "2018-01-01", enddate = "2023-01-01") => {
    // console.log("Get Orders", startdate, enddate);
    if (allOrders) {
      dispatch(getAllOrders({ startdate, enddate }));
    } else {
      dispatch(getUserOrders({ id: user.id, startdate, enddate }));
    }
  };

  useEffect(() => {
    getOrders();
    // eslint-disable-next-line
  }, []);

  // Get Filtered Products (Order Status)
  useEffect(() => {
    if (statusFilter.length > 0) {
      let arr = orders.filter((order) => checkOrder(order));
      // console.log("Filtered", arr);
      setFilteredOrders(arr);
    } else {
      setFilteredOrders(null);
    }
    // eslint-disable-next-line
  }, [statusFilter]);

  const checkOrder = (order) => {
    order.status = "Delivered";
    return statusFilter.find((status) => status === order.status);
  };

  // Get Filtered Products (Date)
  const handleChange = (e) => {
    let date1;
    let date2;
    let startDate;
    let endDate;
    setDateFilter(e.target.value);
    switch (e.target.value) {
      case "All":
        dispatch(getOrders());
        break;

      case "Last 30 days":
        date1 = new Date();
        date1.setDate(date1.getDate() - 30);
        startDate = date1.toISOString().slice(0, 10);

        date2 = new Date();
        date2.setDate(date1.getDate() + 1);
        endDate = date2.toISOString().slice(0, 10);

        dispatch(getOrders(startDate, endDate));

        break;
      case "2020":
        date1 = new Date("2020-01-01");
        startDate = date1.toISOString().slice(0, 10);

        date2 = new Date("2020-12-31");
        endDate = date2.toISOString().slice(0, 10);

        dispatch(getOrders(startDate, endDate));

        break;
      case "2019":
        date1 = new Date("2019-01-01");
        startDate = date1.toISOString().slice(0, 10);

        date2 = new Date("2019-12-31");
        endDate = date2.toISOString().slice(0, 10);

        dispatch(getOrders(startDate, endDate));
        break;
      case "older":
        date1 = new Date("2001-01-01");
        startDate = date1.toISOString().slice(0, 10);

        date2 = new Date("2019-01-01");
        endDate = date2.toISOString().slice(0, 10);

        dispatch(getOrders(startDate, endDate));
        break;
      default:
        break;
    }
  };

  return (
    <>
      <Container className="mt-5 p-3" fluid>
        <Row>
          <Col md={3} style={{ backgroundColor: "white" }} className="p-3">
            <h5 className="mb-0">Filters</h5>
            <hr />
            <div className="p-2">
              <p className="fw-bold">Order status</p>

              {statusArr.map((status, index) => (
                <Form.Group className="my-1" key={index}>
                  <Form.Check
                    type="checkbox"
                    label={status}
                    value={status}
                    onChange={(e) => {
                      if (e.target.checked) {
                        setStatusFilter([...statusFilter, e.target.value]);
                      } else {
                        setStatusFilter(
                          statusFilter.filter((item) => item !== e.target.value)
                        );
                      }
                    }}
                    checked={statusFilter.includes(status)}
                  />
                </Form.Group>
              ))}
            </div>

            <div className="p-2">
              <p className="fw-bold">Order Time</p>

              {dateRangeArr.map((range, index) => (
                <Form.Group className="my-1" key={index}>
                  <Form.Check
                    type="radio"
                    name="date"
                    label={range}
                    value={range}
                    onChange={handleChange}
                    defaultChecked={range === "All"}
                  />
                </Form.Group>
              ))}
            </div>
          </Col>
          <Col md={9}>
            <div className="d-flex justify-content-between border-bottom">
              <h3 className="mb-0">Orders</h3>
              <span
                className="text-muted text-capitalize text-end align-bottom d-flex align-items-end"
                style={{ fontSize: "17px" }}
              >
                (Filters: "{dateFilter}"
                {statusFilter.length > 0 && (
                  <>
                    {" "}
                    ,
                    {statusFilter
                      .map((status, index) => ` "${status}"`)
                      .join(",")}
                  </>
                )}
                )
              </span>
            </div>
            {/* )} */}
            {loading ? (
              <Loading />
            ) : error ? (
              <Message variant="danger">
                <i className="fa fa-warning text-danger" />
                {"  "}
                {error}
              </Message>
            ) : orders.length === 0 ? (
              <Message variant="primary">No Orders Found!</Message>
            ) : (
              <>
                {filteredOrders ? (
                  filteredOrders.length === 0 ? (
                    <Message>No Orders Found!</Message>
                  ) : (
                    filteredOrders.map((item, index) => (
                      <OrderDetails order={item} key={index} />
                    ))
                  )
                ) : (
                  orders.map((item, index) => (
                    <OrderDetails order={item} key={index} />
                  ))
                )}
              </>
            )}
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Orders;

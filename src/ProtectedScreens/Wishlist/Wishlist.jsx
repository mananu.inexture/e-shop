import React from "react";
import Container from "react-bootstrap/esm/Container";
import { Button, Card, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { addToCart, deleteFromWishlist } from "../../Redux/Orders/OrderActions";

import NoItems from "../../Components/NoItems";
import { Link } from "react-router-dom";

function Wishlist() {
  const dispatch = useDispatch();
  const wishlistProducts = useSelector((state) => state.wishList.products);
  // console.log("WISH_LIST", wishlistProducts);
  const user = useSelector((state) => state.userList.authUser);
  const navigate = useNavigate();

  const handleAddtoCart = (product) => {
    if (user) dispatch(addToCart(product, user));
    navigate("/cart");
  };
  const handleRemove = (product) => {
    if (user) dispatch(deleteFromWishlist(product.id));
  };

  return (
    <Container>
      <Card className="my-2 p-3 text-center">
        <h4 className="fw-bold">YOUR WISHLIST</h4>
        <Container>
          {wishlistProducts.length === 0 ? (
            <>
              {" "}
              <NoItems title="Nothing to show in your Wishlist!" />
              <Link to="/">
                <Button variant="dark">Shop now</Button>
              </Link>
            </>
          ) : (
            <Row>
              {wishlistProducts.map((product, index) => (
                <Card className="card m-2 p-3" style={{ width: "25rem" }}>
                  <Card.Img
                    className="align-self-center w-50 mb-4"
                    variant="top"
                    style={{ height: "50%", minHeight: "100px" }}
                    src={product.image}
                  />

                  <Card.Body>
                    <div className="text-center text-dark h-100 d-flex flex-column">
                      <Card.Title className="text-center">
                        {product.title}
                      </Card.Title>
                      <Card.Text className="text-center fw-bolder">
                        &#8377; {Math.round(product.price * 76)}
                      </Card.Text>
                      <div className="row text-center mt-auto">
                        <div className="col-md-6">
                          <Button
                            style={{ width: "150px" }}
                            onClick={() => handleAddtoCart(product)}
                            variant="dark"
                          >
                            Add To Cart
                          </Button>
                        </div>
                        <div className="col-md-6">
                          <Button
                            style={{ width: "150px" }}
                            onClick={() => handleRemove(product)}
                            variant="secondary"
                          >
                            Remove
                          </Button>
                        </div>
                      </div>
                    </div>
                  </Card.Body>
                </Card>
              ))}
            </Row>
          )}
        </Container>
      </Card>
    </Container>
  );
}

export default Wishlist;

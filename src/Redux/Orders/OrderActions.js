import axiosbash from "../../API/FakeStoreAPI";
import {
  ADD_ORDER_FAIL,
  ADD_ORDER_REQUEST,
  ADD_ORDER_SUCCESS,
  DELETE_FROM_CART,
  DELETE_ORDER_FAIL,
  DELETE_ORDER_REQUEST,
  DELETE_ORDER_SUCCESS,
  GET_ORDERS_FAIL,
  GET_ORDERS_REQUEST,
  GET_ORDERS_SUCCESS,
  GET_SINGLE_ORDER_FAIL,
  GET_SINGLE_ORDER_REQUEST,
  GET_SINGLE_ORDER_SUCCESS,
  ADD_TO_CART,
  UPDATE_ORDER_FAIL,
  UPDATE_ORDER_REQUEST,
  UPDATE_ORDER_SUCCESS,
  EMPTY_CART,
  SET_QUANTITY,
  SET_ADDRESS,
  ADD_TO_WISHLIST,
  DELETE_FROM_WISHLIST,
} from "./OrderTypes";

// Add to Wishlist
export const addToWishlist = (product) => {
  product.qty = 1;
  return {
    type: ADD_TO_WISHLIST,
    payload: product,
  };
};

// Delete from Wishlist
export const deleteFromWishlist = (id) => {
  return {
    type: DELETE_FROM_WISHLIST,
    payload: id,
  };
};

// Add to cart
export const addToCart = (product, user) => {
  product.qty = 1;
  return {
    type: ADD_TO_CART,
    payload: { ...product, user },
  };
};

//SET QUANTITY
export const setQuantity = (id, qty = 1) => {
  return {
    type: SET_QUANTITY,
    payload: { id, qty },
  };
};

// Delete from cart
export const deleteFromCart = (id) => {
  return {
    type: DELETE_FROM_CART,
    payload: id,
  };
};

// Empty cart
export const emptyCart = () => {
  return {
    type: EMPTY_CART,
  };
};

// Set Address
export const setAddress = (address) => {
  return {
    type: SET_ADDRESS,
    payload: address,
  };
};

// Get all orders
export const getAllOrders =
  ({
    sort = "asc",
    limit = 100,
    startdate = "2019-12-10",
    enddate = "2023-10-10",
  }) =>
  async (dispatch) => {
    dispatch({
      type: GET_ORDERS_REQUEST,
    });

    try {
      const res = await axiosbash.get(
        `/carts?sort=${sort}${limit ? `&&limit=${limit}` : ""}${
          startdate ? `&&startdate=${startdate}` : ""
        }${enddate ? `&&enddate=${enddate}` : ""}`
      );

      dispatch({
        type: GET_ORDERS_SUCCESS,
        payload: res.data,
      });
    } catch (err) {
      dispatch({
        type: GET_ORDERS_FAIL,
        payload: err.response ? err.response.data : err.message,
      });
    }
  };

// Get a single orders
export const getSingleOrder = (id) => async (dispatch) => {
  dispatch({
    type: GET_SINGLE_ORDER_REQUEST,
  });

  try {
    const res = await axiosbash.get(`/carts/${id}`);

    dispatch({
      type: GET_SINGLE_ORDER_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: GET_SINGLE_ORDER_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

// // Get orders by date range
// export const getOrderByDate = (start, end) => async (dispatch) => {
//   dispatch({
//     type: GET_ORDERS_REQUEST,
//   });
//   try {
//     const res = await axiosbash.get(`/carts/startdate=${start}&enddate=${end}`);

//     dispatch({
//       type: GET_ORDERS_SUCCESS,
//       payload: res.data,
//     });
//   } catch (err) {
//     dispatch({
//       type: GET_ORDERS_FAIL,
//       payload: err.response ? err.response.data : err.message,
//     });
//   }
// };

// Get user orders
export const getUserOrders =
  ({
    id,
    sort = "desc",
    limit = 100,
    startdate = "2010-12-10",
    enddate = "2023-10-10",
  }) =>
  async (dispatch) => {
    // console.log("Get orders by Id:", id, startdate, enddate);

    dispatch({
      type: GET_ORDERS_REQUEST,
    });
    try {
      const res = await axiosbash.get(
        `/carts/user/${id}?sort=${sort}${limit ? `&&limit=${limit}` : ""}${
          startdate ? `&&startdate=${startdate}` : ""
        }${enddate ? `&&enddate=${enddate}` : ""}`
      );
      // console.log(res.data);

      dispatch({
        type: GET_ORDERS_SUCCESS,
        payload: res.data,
      });
    } catch (err) {
      dispatch({
        type: GET_ORDERS_FAIL,
        payload: err.response ? err.response.data : err.message,
      });
    }
  };

// Add order
export const addOrder = (cart) => async (dispatch) => {
  let date = new Date();
  date = date.toISOString();
  const data = {
    userId: cart.userId,
    date: date,
    products: cart.products.map((product) => ({
      productId: product.id,
      quantity: product.qty,
    })),
  };

  dispatch({
    type: ADD_ORDER_REQUEST,
  });
  try {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    const res = await axiosbash.post(`/carts`, data, config);
    // console.log(res.data);

    dispatch({
      type: ADD_ORDER_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: ADD_ORDER_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

// Add order
export const updateOrder = () => async (dispatch) => {
  const data = {
    userId: 5,
    date: "2020-02-03",
    products: [
      { productId: 5, quantity: 3 },
      { productId: 1, quantity: 5 },
    ],
  };

  dispatch({
    type: UPDATE_ORDER_REQUEST,
  });
  try {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    const res = await axiosbash.put(`/carts`, data, config);

    dispatch({
      type: UPDATE_ORDER_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: UPDATE_ORDER_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

// Delete order
export const deleteOrder = (id) => async (dispatch) => {
  dispatch({
    type: DELETE_ORDER_REQUEST,
  });
  try {
    await axiosbash.delete(`/carts/${id}`);

    dispatch({
      type: DELETE_ORDER_SUCCESS,
      payload: id,
    });
  } catch (err) {
    dispatch({
      type: DELETE_ORDER_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

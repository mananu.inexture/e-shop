import {
  DELETE_FROM_CART,
  GET_ORDERS_FAIL,
  GET_ORDERS_REQUEST,
  GET_ORDERS_SUCCESS,
  GET_SINGLE_ORDER_FAIL,
  GET_SINGLE_ORDER_REQUEST,
  GET_SINGLE_ORDER_SUCCESS,
  ADD_TO_CART,
  EMPTY_CART,
  SET_QUANTITY,
  SET_ADDRESS,
  ADD_ORDER_SUCCESS,
  ADD_ORDER_REQUEST,
  ADD_ORDER_FAIL,
  DELETE_ORDER_SUCCESS,
  DELETE_ORDER_FAIL,
  DELETE_ORDER_REQUEST,
  ADD_TO_WISHLIST,
  DELETE_FROM_WISHLIST,
} from "./OrderTypes";

import { decrypt, encrypt } from "../../Utils/Encrypt";

let cart = localStorage.getItem("cart");
let wishlist = localStorage.getItem("wishlist");

export const wishlistReducer = (
  state = { products: wishlist ? decrypt(wishlist) : [] },
  action
) => {
  switch (action.type) {
    case ADD_TO_WISHLIST:
      let wishlistProducts = state.products.find(
        (item) => item.id === action.payload.id
      )
        ? state.products
        : [...state.products, action.payload];

      localStorage.setItem("wishlist", encrypt(wishlistProducts));
      return {
        ...state,
        products: wishlistProducts,
      };

    case DELETE_FROM_WISHLIST:
      const filteredWishlist = state.products.filter(
        (item) => item.id !== action.payload
      );
      filteredWishlist.length === 0
        ? localStorage.removeItem("wishlist")
        : localStorage.setItem("wishlist", encrypt(filteredWishlist));
      return {
        ...state,
        products: filteredWishlist,
      };

    default:
      return state;
  }
};

export const cartReducer = (
  state = {
    userId: null,
    address: "",
    products: cart ? decrypt(cart) : [],
  },
  action
) => {
  switch (action.type) {
    case ADD_TO_CART:
      const cartProducts = state.products.find(
        (item) => item.id === action.payload.id
      )
        ? state.products.map((item) => {
            if (item.id === action.payload.id) {
              item.qty = item.qty + 1;
              return item;
            } else {
              return item;
            }
          })
        : [...state.products, action.payload];

      localStorage.setItem("cart", encrypt(cartProducts));
      return {
        ...state,
        products: cartProducts,
        userId: action.payload.user.id,
      };

    case SET_QUANTITY:
      const products = state.products.map((item) => {
        if (item.id === action.payload.id) {
          item.qty = action.payload.qty;
          return item;
        } else {
          return item;
        }
      });
      localStorage.setItem("cart", encrypt(products));
      return {
        ...state,
        products: products,
      };

    case DELETE_FROM_CART:
      const filteredProducts = state.products.filter(
        (item) => item.id !== action.payload
      );
      localStorage.setItem("cart", encrypt(filteredProducts));
      return {
        ...state,
        products: filteredProducts,
      };

    case SET_ADDRESS:
      return {
        ...state,
        address: action.payload,
      };

    case EMPTY_CART:
      localStorage.removeItem("cart");
      return {
        ...state,
        products: [],
      };

    default:
      return state;
  }
};

export const orderListReducer = (
  state = {
    currentOrder: null,
    orders: [],
  },
  action
) => {
  switch (action.type) {
    case GET_ORDERS_REQUEST:
    case GET_SINGLE_ORDER_REQUEST:
    case ADD_ORDER_REQUEST:
    case DELETE_ORDER_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_ORDERS_SUCCESS:
      return {
        ...state,
        orders: action.payload,
        loading: false,
      };
    case GET_SINGLE_ORDER_SUCCESS:
      return {
        ...state,
        currentOrder: action.payload,
        loading: false,
      };
    case ADD_ORDER_SUCCESS:
      return {
        ...state,
        currentOrder: action.payload,
        loading: false,
      };
    case DELETE_ORDER_SUCCESS:
      return {
        ...state,
        orders: state.orders.filter((order) => order.id !== action.payload),
        loading: false,
      };
    case GET_ORDERS_FAIL:
    case GET_SINGLE_ORDER_FAIL:
    case ADD_ORDER_FAIL:
    case DELETE_ORDER_FAIL:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};

import axiosbash from "../../API/FakeStoreAPI";
import {
  ADD_CATEGORY,
  ADD_PRODUCT_FAIL,
  ADD_PRODUCT_REQUEST,
  ADD_PRODUCT_SUCCESS,
  CLEAR_FILTER,
  DELETE_CATEGORY,
  DELETE_PRODUCT_FAIL,
  DELETE_PRODUCT_REQUEST,
  DELETE_PRODUCT_SUCCESS,
  GET_CATEGORIES_FAIL,
  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_PRODUCTS_BY_CATEGORY_FAIL,
  GET_PRODUCTS_BY_CATEGORY_REQUEST,
  GET_PRODUCTS_BY_CATEGORY_SUCCESS,
  GET_PRODUCTS_FAIL,
  GET_PRODUCTS_REQUEST,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_DETAILS_FAIL,
  GET_PRODUCT_DETAILS_REQUEST,
  GET_PRODUCT_DETAILS_SUCCESS,
  SET_FILTER,
  SET_FILTERED_PRODUCTS,
  UPDATE_CATEGORY,
  UPDATE_PRODUCT_FAIL,
  UPDATE_PRODUCT_REQUEST,
  UPDATE_PRODUCT_SUCCESS,
} from "./ProductTypes";

// Get All Categories
export const getCategories = () => async (dispatch) => {
  dispatch({
    type: GET_CATEGORIES_REQUEST,
  });

  try {
    const res = await axiosbash.get("/products/categories");

    dispatch({
      type: GET_CATEGORIES_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: GET_CATEGORIES_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

// Get All Products
export const getProducts = () => async (dispatch) => {
  dispatch({
    type: GET_PRODUCTS_REQUEST,
  });

  try {
    const res = await axiosbash.get("/products");
    let data = res.data.map((product) => {
      if (!product.rating) {
        product.rating = { rate: 0, count: 0 };
      }
      if (!product.price) {
        product.price = 0.0;
      }
      return product;
    });
    dispatch({
      type: GET_PRODUCTS_SUCCESS,
      payload: data,
    });
  } catch (err) {
    dispatch({
      type: GET_PRODUCTS_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

// Get Products by Category
export const getProductsByCategory =
  (category, limit = null) =>
  async (dispatch) => {
    dispatch({
      type: GET_PRODUCTS_BY_CATEGORY_REQUEST,
    });

    try {
      const res = await axiosbash.get(
        `/products/category/${category}${limit ? `?limit=${limit}` : ""}`
      );

      // console.log({ [category]: res.data });
      dispatch({
        type: GET_PRODUCTS_BY_CATEGORY_SUCCESS,
        payload: { category, products: res.data },
      });
    } catch (err) {
      dispatch({
        type: GET_PRODUCTS_BY_CATEGORY_FAIL,
        payload: err.response ? err.response.data : err.message,
      });
    }
  };

// Get a Single Product
export const getProductDetails = (id) => async (dispatch) => {
  dispatch({
    type: GET_PRODUCT_DETAILS_REQUEST,
  });

  try {
    const res = await axiosbash.get(`/products/${id}`);

    dispatch({
      type: GET_PRODUCT_DETAILS_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
    dispatch({
      type: GET_PRODUCT_DETAILS_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

// Add a Product
export const addProduct =
  (formData, navigate = null) =>
  async (dispatch) => {
    const data = {
      title: formData.title,
      price: formData.price,
      description: formData.description,
      image: formData.image,
      category: formData.category,
      // rating: {rate:formData.rate, count: formData.count}
    };
    // console.log(data);
    dispatch({
      type: ADD_PRODUCT_REQUEST,
    });

    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };

      const res = await axiosbash.post(`/products`, data, config);

      // console.log(res.data);
      dispatch({
        type: ADD_PRODUCT_SUCCESS,
        payload: { category: data.category, product: res.data },
      });
      if (navigate) navigate("/products");
    } catch (err) {
      dispatch({
        type: ADD_PRODUCT_FAIL,
        payload: err.response ? err.response.data : err.message,
      });
    }
  };

// Update a Product
export const updateProduct = (id, formData) => async (dispatch) => {
  dispatch({
    type: UPDATE_PRODUCT_REQUEST,
  });

  try {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const res = await axiosbash.put(`/products/${id}`, formData, config);

    // console.log(res.data);
    dispatch({
      type: UPDATE_PRODUCT_SUCCESS,
      payload: { category: formData.category, updatedProduct: res.data },
    });
  } catch (err) {
    // console.log(err);
    dispatch({
      type: UPDATE_PRODUCT_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

// Delete a Product
export const deleteProduct = (id, category) => async (dispatch) => {
  // const id = 9;
  // const category = "electronics";

  dispatch({
    type: DELETE_PRODUCT_REQUEST,
  });

  try {
    await axiosbash.delete(`/products/${id}`);

    dispatch({
      type: DELETE_PRODUCT_SUCCESS,
      payload: { category, id },
    });
  } catch (err) {
    // console.log(err);
    dispatch({
      type: DELETE_PRODUCT_FAIL,
      payload: err.response ? err.response.data : err.message,
    });
  }
};

// Set filter
export const setFilter = (text) => {
  return {
    type: SET_FILTER,
    payload: text,
  };
};

// Set filtered products
export const setFilteredProducts = (text) => {
  return {
    type: SET_FILTERED_PRODUCTS,
    payload: text,
  };
};

// Clear filtered products
export const clearFilter = () => {
  return {
    type: CLEAR_FILTER,
  };
};

// Add category
export const addCategory = (category) => {
  return {
    type: ADD_CATEGORY,
    payload: category,
  };
};

// Add category
export const updateCategory = (id, category) => {
  // console.log(id, category);
  return {
    type: UPDATE_CATEGORY,
    payload: { id, category },
  };
};

// Remove category
export const deleteCategory = (id) => {
  return {
    type: DELETE_CATEGORY,
    payload: id,
  };
};

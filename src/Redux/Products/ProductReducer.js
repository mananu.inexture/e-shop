// import productData from "../../assets/data/products";
import {
  ADD_PRODUCT_FAIL,
  ADD_PRODUCT_REQUEST,
  ADD_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAIL,
  DELETE_PRODUCT_REQUEST,
  DELETE_PRODUCT_SUCCESS,
  GET_CATEGORIES_FAIL,
  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_PRODUCTS_BY_CATEGORY_FAIL,
  GET_PRODUCTS_BY_CATEGORY_REQUEST,
  GET_PRODUCTS_BY_CATEGORY_SUCCESS,
  GET_PRODUCTS_FAIL,
  GET_PRODUCTS_REQUEST,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_DETAILS_FAIL,
  GET_PRODUCT_DETAILS_REQUEST,
  GET_PRODUCT_DETAILS_SUCCESS,
  SET_FILTER,
  CLEAR_FILTER,
  SET_FILTERED_PRODUCTS,
  UPDATE_PRODUCT_FAIL,
  UPDATE_PRODUCT_REQUEST,
  UPDATE_PRODUCT_SUCCESS,
  ADD_CATEGORY,
  DELETE_CATEGORY,
  UPDATE_CATEGORY,
} from "./ProductTypes";

// Category List
export const categoryListReducer = (state = { categories: [] }, action) => {
  switch (action.type) {
    case GET_CATEGORIES_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.payload,
        loading: false,
      };
    case ADD_CATEGORY:
      return {
        ...state,
        categories: [...state.categories, action.payload],
        loading: false,
      };
    case UPDATE_CATEGORY:
      // console.log(action.payload);
      return {
        ...state,
        categories: state.categories.map((category, index) =>
          index === action.payload.id ? action.payload.category : category
        ),
        loading: false,
      };
    case DELETE_CATEGORY:
      return {
        ...state,
        categories: state.categories.filter(
          (category, index) => index !== action.payload
        ),
        loading: false,
      };

    case GET_CATEGORIES_FAIL:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};

// Products per Category
export const categoryProductsReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_PRODUCTS_BY_CATEGORY_REQUEST:
    case ADD_PRODUCT_REQUEST:
    case UPDATE_PRODUCT_REQUEST:
    case DELETE_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case GET_PRODUCTS_BY_CATEGORY_SUCCESS:
      return {
        ...state,
        [action.payload.category]: action.payload.products,
        loading: false,
      };

    case ADD_PRODUCT_SUCCESS:
      const { category, product } = action.payload;
      return {
        ...state,
        [category]: state[category] ? [...state[category], product] : [product],
        loading: false,
      };

    case UPDATE_PRODUCT_SUCCESS:
      const { category: updatedCategory, updatedProduct } = action.payload;
      return {
        ...state,
        [updatedCategory]: state[updatedCategory]
          ? state[updatedCategory].map((product) =>
              product.id === updatedProduct.id ? updatedProduct : product
            )
          : [updatedProduct],
        loading: false,
      };

    case DELETE_PRODUCT_SUCCESS:
      const { category: deletedCategory, id } = action.payload;
      return {
        ...state,
        [deletedCategory]:
          state[deletedCategory] &&
          state[deletedCategory].filter((product) => product.id !== id),
        loading: false,
      };

    case GET_PRODUCTS_BY_CATEGORY_FAIL:
    case ADD_PRODUCT_FAIL:
    case UPDATE_PRODUCT_FAIL:
    case DELETE_PRODUCT_FAIL:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};

// All Products List
export const   productListReducer = (
  state = { products: [], filter: "", filteredProducts: null },
  action
) => {
  switch (action.type) {
    case GET_PRODUCTS_REQUEST:
    case ADD_PRODUCT_REQUEST:
    case UPDATE_PRODUCT_REQUEST:
    case DELETE_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.payload,
        loading: false,
      };
    case ADD_PRODUCT_SUCCESS:
      const { product } = action.payload;
      return {
        ...state,
        products: [...state.products, product],
        loading: false,
      };
    case UPDATE_PRODUCT_SUCCESS:
      const { updatedProduct } = action.payload;
      return {
        ...state,
        products: state.products.map((product) =>
          product.id === updatedProduct.id ? updatedProduct : product
        ),
        loading: false,
      };

    case DELETE_PRODUCT_SUCCESS:
      const { id } = action.payload;
      return {
        ...state,
        products: state.products.filter((product) => product.id !== id),
        loading: false,
      };

    case SET_FILTER:
      return {
        ...state,
        filter: action.payload,
      };

    case SET_FILTERED_PRODUCTS:
      return {
        ...state,
        filteredProducts: state.products.filter(
          (product) =>
            product.title.toLowerCase().includes(action.payload.toLowerCase()) ||
            product.description.toLowerCase().includes(action.payload.toLowerCase()) ||
            product.category.toLowerCase().includes(action.payload.toLowerCase())
        ),
      };
    case CLEAR_FILTER:
      return {
        ...state,
        filter: "",
        filteredProducts: null,
      };
    case GET_PRODUCTS_FAIL:
    case ADD_PRODUCT_FAIL:
    case UPDATE_PRODUCT_FAIL:
    case DELETE_PRODUCT_FAIL:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};

// Single Product Details
export const productDetailsReducer = (state = { product: null }, action) => {
  switch (action.type) {
    case GET_PRODUCT_DETAILS_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case GET_PRODUCT_DETAILS_SUCCESS:
      return {
        ...state,
        product: action.payload,
        loading: false,
      };
    case GET_PRODUCT_DETAILS_FAIL:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};

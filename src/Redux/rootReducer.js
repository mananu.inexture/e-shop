import { combineReducers } from "redux";
import { cartReducer, orderListReducer, wishlistReducer } from "./Orders/OrderReducer";
import {
  categoryListReducer,
  categoryProductsReducer,
  productDetailsReducer,
  productListReducer,
} from "./Products/ProductReducer";
import { userReducer } from "./Users/UserReducer";

const rootReducer = combineReducers({
  productList: productListReducer,
  categoryList: categoryListReducer,
  productDetails: productDetailsReducer,
  categoryProducts: categoryProductsReducer,
  wishList:wishlistReducer,
  cartList: cartReducer,
  orderList: orderListReducer,
  userList: userReducer,
});

export default rootReducer;

import React from "react";
import { useSelector } from "react-redux";
import { Outlet, Navigate } from "react-router-dom";

const PrivateRoute = ({ checkAdmin = false }) => {
  const user = useSelector((state) => state.userList.authUser);
  const isAdmin = user?.isAdmin;

  return checkAdmin ? (
    isAdmin ? (
      <Outlet />
    ) : (
      <Navigate to="/" />
    )
  ) : user ? (
    <Outlet />
  ) : (
    <Navigate to="/" />
  );
};

export default PrivateRoute;

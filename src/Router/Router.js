import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getUsers } from "../Redux/Users/UserAction";
import { getProducts } from "../Redux/Products/ProductActions";

// User
import Login from "../Screens/Login/Login";
import Signup from "../Screens/Signup/Signup";
import Header from "../Components/Header/Header";
import Dashboard from "../Screens/Home/Dashboard";
import UserProfile from "../ProtectedScreens/UserProfile/UserProfile";
import Cart from "../ProtectedScreens/Cart/Cart";
import ProceedCheckout from "../ProtectedScreens/Cart/ProceedCheckout";
import Shipped from "../ProtectedScreens/Cart/Shipped";
import DispProductList from "../Screens/ProductListing/DispProductList";
import ProductDetails from "../Screens/ProductDetails/ProductDetails";
import Error404 from "../Screens/Error404/Error404";
import Orders from "../ProtectedScreens/Orders/Orders";
import Wishlist from "../ProtectedScreens/Wishlist/Wishlist";

// Admin
import AdminDashboard from "../ProtectedScreens/Admin/Dashboard/AdminDashboard";
import AdminProfile from "../ProtectedScreens/Admin/AdminProfile";
import Sidebar from "../ProtectedScreens/Admin/Components/Drawer/Sidebar";
import UserList from "../ProtectedScreens/Admin/Users/UserList";
import OrderList from "../ProtectedScreens/Admin/Orders/OrderList";
import ProductList from "../ProtectedScreens/Admin/Products/ProductList";
import CategoryList from "../ProtectedScreens/Admin/Categories/CategoryList";

// Utils
import PrivateRoute from "./PrivateRoute";

const Router = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUsers());
    dispatch(getProducts());

    // dispatch(getSingleUser(3));
    // productSeeder();
    // dispatch(deleteProduct(19, "electronics"));
    // eslint-disable-next-line
  }, []);

  // Add product data to server
  // const productSeeder = async () => {
  //   const res = await axios.get("/AllProducts.json");
  //   res.data.map((product) => dispatch(addProduct(product)));
  // };

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Header />}>
            <Route index element={<Dashboard />} />
            <Route path="product/:id" element={<ProductDetails />} />
            <Route path="*" element={<Error404 />} />
            <Route path="products" element={<DispProductList />} />
            <Route path="login" element={<Login />} />
            <Route path="signup" element={<Signup />} />

            {/* Private */}
            <Route element={<PrivateRoute />}>
              <Route path="/cart" element={<Cart />} />
              <Route path="/profile" element={<UserProfile />} />
              <Route path="/checkout" element={<ProceedCheckout />} />
              <Route path="/shipped" element={<Shipped />} />
              <Route path="/orders" element={<Orders />} />
              <Route path="/wishlist" element={<Wishlist />} />
            </Route>
          </Route>

          {/* Admin */}
          <Route element={<PrivateRoute checkAdmin={true} />}>
            <Route path="/admin" element={<Sidebar />}>
              <Route index element={<AdminDashboard />} />
              <Route path="profile" element={<AdminProfile />} />
              <Route path="dashboard" element={<AdminDashboard />} />
              <Route path="users" element={<UserList />} />
              <Route path="products" element={<ProductList />} />
              <Route path="orders" element={<OrderList />} />
              <Route path="categories" element={<CategoryList />} />
            </Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default Router;

import React from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import pageNotFound from "../../Assets/Images/404.gif";
function Error404() {

  const navigate = useNavigate();

  return (
    <Container>
      <Row>
        <Col md={{ span: 8, offset: 2 }}>
          <div className="d-flex flex-column align-items-center">
            <img src={pageNotFound} className="m-auto img-fluid w-75" alt="Error" />
          </div>
          <h2 className="text-info mt-4">Looking for something?</h2>
          <p className="text-dark fs-4">We're sorry. The Web address you entered is not a functioning page on our site.</p>
          <div className="navlink mb-2 text-center text-decoration-none mx-2">
            <small className="text-dark fs-4">Go to shopee mall home page
              <Button variant="outline-dark mx-4" onClick={() => navigate("/", { replace: true })}>Go home</Button></small>
          </div>
        </Col>
      </Row>
    </Container >
  );
}

export default Error404;

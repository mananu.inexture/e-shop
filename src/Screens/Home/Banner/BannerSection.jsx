import React from "react";
import { Carousel, Container } from "react-bootstrap";
import "./BannerSection.css";

const BannerSection = () => {
  //Banner images list array to iterate loop for images in carousel-item.
  const bannerImgList = [
    "https://cdn.shopclues.com/images/banners/2022/HB5_WallSticker_Web_MK5_03Apr22.jpg",
    "https://cdn.shopclues.com/images/banners/2022/HB3_AccessoriesStore_Web_SYM_03April22.jpg",
    "https://dcassetcdn.com/design_img/2690773/17651/17651_14601612_2690773_f2a2a98b_image.jpg",
    "https://mir-s3-cdn-cf.behance.net/project_modules/1400_opt_1/ba55b599158985.6054d057968a0.jpg",
    "https://cdn.shopclues.com/images/banners/2022/3April_PrimeMall_HB2_W_VR.jpg",
    "https://canvs-prod.s3-ap-southeast-1.amazonaws.com/media/artwork/ced04f05-6ed1-4ea7-a262-eac9dfb6d376.jpg",
    "https://cdn.shopclues.com/images/banners/HB4_TopRated_Web_MK5_06Mar22.jpg",
    "https://mir-s3-cdn-cf.behance.net/project_modules/1400_opt_1/1da67199158985.6054d057946b5.jpg",
    "https://cdn.shopclues.com/images/banners/2022/HB1_SFM_Web_MK5_03Apr22R.jpg",
    "https://mir-s3-cdn-cf.behance.net/project_modules/1400_opt_1/fd46b399158985.6054d05793881.jpg",
    "https://cdn5.f-cdn.com/contestentries/1291972/22894184/5ac47fefdbb2e_thumb900.jpg",
    "https://mir-s3-cdn-cf.behance.net/project_modules/1400_opt_1/e7acef99158985.5f5d051058be9.jpg"
  ];

  return (
    <Container fluid className="banner p-0">
      <Carousel className="h-25" indicators={false} interval={2000}>
        {bannerImgList.map((eachData, index) => (
          <Carousel.Item key={`Banner_${index}`}>
            <img className="d-block w-100 banner-img" src={eachData} alt="First slide" height="500" />
          </Carousel.Item>
        ))}
      </Carousel>
    </Container>
  );
};

export default BannerSection;
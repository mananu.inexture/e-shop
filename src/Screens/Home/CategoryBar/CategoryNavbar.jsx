import React from "react";
import { Container, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./CategoryNavbar.css";

const CategoryNavbar = ({ catName }) => {
  //If server is down or catName array is empty then default array list of categories will be stored in catName props.
  catName =
    catName.length > 0
      ? catName
      : [
          "electronics",
          "jewelery",
          "men's clothing",
          "women's clothing",
          "electronics",
          "jewelery",
          "men's clothing",
          "women's clothing",
        ];

  return (
    <Container fluid className="categoryNav p-2">
      <Navbar className="justify-content-center align-items-center">
        {catName.map((item, index) => (
          <Link
            to={`/products`}
            state={{ catName: item }}
            className="mx-4 text-decoration-none fw-bold fs-5 text-black"
            key={`category_${index}`}
          >
            <span className="text-capitalize">{item}</span>
          </Link>
        ))}
      </Navbar>
    </Container>
  );
};

export default CategoryNavbar;

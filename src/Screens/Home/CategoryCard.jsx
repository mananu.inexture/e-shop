import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getProductsByCategory } from "../../Redux/Products/ProductActions";
import ProductCard from "../../Components/ProductCard";
import { Button, Col, Container, Row } from "react-bootstrap";
import Loading from "../../Components/Loading";
import Message from "../../Components/Message";

const CategoryCard = ({ catName }) => {
  const categoryProducts = useSelector((state) => state.categoryProducts);
  const { loading, error } = categoryProducts;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProductsByCategory(catName, 4));
    // eslint-disable-next-line
  }, [catName]);
  return (
    <>
      <Container className="my-4 p-3 bg-white">
        <Row className="align-items-center">
          <Col className="fs-3">{catName.toUpperCase()}</Col>
          <Col className="text-end">
            <Link to={`/products`} state={{ catName }}>
              <Button variant="dark" style={{ width: "100px" }}>
                View all
              </Button>
            </Link>
          </Col>
        </Row>

        <Row>
          {loading || !categoryProducts[catName] ? (
            <Loading />
          ) : error ? (
            <Message variant="danger">
              <i className="fa fa-warning text-danger" />
              {"  "}
              {error}
            </Message>
          ) : (
            categoryProducts[catName].map((product, index) => (
              <ProductCard product={product} key={`${catName}_${index}`} />
            ))
          )}
        </Row>
      </Container>
    </>
  );
};

export default CategoryCard;

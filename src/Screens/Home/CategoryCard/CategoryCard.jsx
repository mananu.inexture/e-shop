import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProductsByCategory } from "../../../Redux/Products/ProductActions";
import ProductCard from "../../../Components/ProductCard/ProductCard";
import { Button, Card, CardGroup, Container, Row } from "react-bootstrap";
import Loading from "../../../Components/Loading";
import Message from "../../../Components/Message";
import { NavLink } from "react-router-dom";
import "./CategoryCard.css";

const CategoryCard = ({ catName }) => {
  const categoryProducts = useSelector((state) => state.categoryProducts);
  const { loading, error } = categoryProducts;

  // console.log(categoryProducts);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProductsByCategory(catName, 4));
    // eslint-disable-next-line
  }, [catName]);

  return (
    <Container fluid className="py-3 position-relative category-card">
      <Row md={12} className="px-3">
        {loading || !categoryProducts[catName] ? (
          <Loading />
        ) : error ? (
          <Message variant="warning">{error}</Message>
        ) : (
          <CardGroup className="border-0 p-0">
            {categoryProducts[catName].map((product, index) => (
              <ProductCard product={product} key={`${product.id}_${index}`} />
            ))}
            <Row className="g-0" style={{ width: "12%" }}>
              <Card className="border-0 rounded-end text-center vall-card product-card">
                <Card.Header>
                  <Card.Title className="fs-5 pt-3 fw-bold mb-3">{catName.toUpperCase()}</Card.Title>
                </Card.Header>
                <Card.Body className="d-flex flex-column justify-content-center">
                  <NavLink to={`/products`} state={{ catName }} className="bi bi-arrow-right-circle-fill fw-bold vall-icon"></NavLink>
                </Card.Body>
                <NavLink to={`/products`} state={{ catName }} className="pb-5">
                  <Button variant="dark w-75">View all</Button>
                </NavLink>
              </Card >
            </Row>
          </CardGroup>
        )}
      </Row>
    </Container >
  );
};

export default CategoryCard;

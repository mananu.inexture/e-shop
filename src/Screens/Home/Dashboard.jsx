import React, { useEffect } from "react";
import BannerSection from "./Banner/BannerSection";
import CategoryNavbar from "./CategoryBar/CategoryNavbar";
import CategoryCard from "./CategoryCard/CategoryCard";
import { useDispatch, useSelector } from "react-redux";
import { getCategories } from "../../Redux/Products/ProductActions";
import Loading from "../../Components/Loading";
import Message from "../../Components/Message";
import NoItems from "../../Components/NoItems";
import { useNavigate } from "react-router-dom";

function Dashboard() {
  const dispatch = useDispatch();

  const categoryList = useSelector((state) => state.categoryList);
  const { categories, loading, error } = categoryList;

  const filteredProducts = useSelector(
    (state) => state.productList.filteredProducts
  );

  useEffect(() => {
    dispatch(getCategories());
    // eslint-disable-next-line
  }, []);

  const navigate = useNavigate();
  useEffect(() => {
    if (filteredProducts) {
      navigate("/products");
    }
    // eslint-disable-next-line
  }, [filteredProducts]);

  return loading ? (
    <Loading size={100} />
  ) : error ? (
    <Message variant="danger">
      <i className="fa fa-warning text-danger" />
      {"  "}
      {error}
    </Message>
  ) : categories.length === 0 ? (
    <NoItems title="No Products Found." />
  ) : (
    <>
      <CategoryNavbar catName={categories} />
      <BannerSection />
      {categories.map((category, index) => (
        <CategoryCard catName={category} key={`category_${index}`} />
      ))}
    </>
  );
}

export default Dashboard;

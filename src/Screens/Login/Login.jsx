import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import Loading from "../../Components/Loading";
import Message from "../../Components/Message";
import { loginUser } from "../../Redux/Users/UserAction";
import login_img from "../../Assets/Images/login.svg";
import {
  Button,
  Col,
  Container,
  FloatingLabel,
  Form,
  Row,
} from "react-bootstrap";
import { yupResolver } from "@hookform/resolvers/yup";
import { loginSchema } from "../../Validations/index";

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(loginSchema),
  });

  const userList = useSelector((state) => state.userList);
  const { loading, error, authUser } = userList;

  useEffect(() => {
    if (authUser) {
      if (authUser.isAdmin) {
        navigate("/admin");
      } else {
        navigate("/");
      }
    }
    // eslint-disable-next-line....
  }, [authUser]);

  const onSubmit = (data) => {
    // console.log(data);
    dispatch(loginUser(data, navigate));
  };
  return (
    <Container>
      {loading && <Loading />}
      <Row className="mt-5 bg-light p-5 mb-5">
        {error && (
          <Message variant="danger">
            <i className="fa fa-warning text-danger" />
            {"  "}
            {error}
          </Message>
        )}
        <Col md={5}>
          <h3 className="text-center fw-bold mb-3">Sign In</h3>
          <hr />
          <Form onSubmit={handleSubmit(onSubmit)}>
            {/* Userame*/}
            <FloatingLabel label="Username" className="mt-3">
              <Form.Control
                {...register("username")}
                type="text"
                placeholder="Username"
              />
            </FloatingLabel>
            {errors.username && (
              <small className="text-danger fs-5">
                {errors.username.message}
              </small>
            )}

            {/* Password */}
            <FloatingLabel label="Password" className="mt-3">
              <Form.Control
                {...register("password")}
                type="password"
                placeholder="Password"
              />
            </FloatingLabel>
            {errors.password && (
              <small className="text-danger fs-5">
                {errors.password.message}
              </small>
            )}
            {/* checkBox */}
            <Form.Group className="my-3">
              <Form.Check
                {...register("remember")}
                type="checkbox"
                label="Remember me"
              />
            </Form.Group>

            <Button type="submit" variant="dark" className=" w-100">
              Login
            </Button>
            <p className="mt-3">
              Don't have an account ? <Link to="/signup">Sign up</Link>
            </p>
          </Form>
        </Col>
        <Col md={7}>
          <img className="mt-5 h-50 w-100" src={login_img} alt="login" />
        </Col>
      </Row>
    </Container>
  );
}

export default Login;

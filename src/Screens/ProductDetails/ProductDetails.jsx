import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";
import { getProductDetails } from "../../Redux/Products/ProductActions";
import { addToCart, addToWishlist } from "../../Redux/Orders/OrderActions";
import "./ProductDetails.css";
import Loading from "../../Components/Loading";
import Message from "../../Components/Message";
import { Button, Col, Container, Row } from "react-bootstrap";

function Product_details() {
  const params = useParams();
  const id = params.id;

  const productDetails = useSelector((state) => state.productDetails);
  const { product, loading, error } = productDetails;

  const user = useSelector((state) => state.userList.authUser);

  const dispatch = useDispatch();

  const navigate = useNavigate();

  const handleAdd = () => {
    if (user) dispatch(addToCart(product, user));
    navigate("/cart");
  };
  const handleWishList = () => {
    if (user) dispatch(addToWishlist(product));
    navigate("/wishlist");
  };

  const handleBuyNow = () => {
    if (user) dispatch(addToCart(product, user));
    navigate("/checkout");
  };

  useEffect(() => {
    dispatch(getProductDetails(id));
    // eslint-disable-next-line
  }, [id]);

  // TODO - Add "Product Not Found Card! (!product)"
  return loading ? (
    <Loading size={100} />
  ) : error ? (
    <Message variant="danger">
      <i className="fa fa-warning text-danger" />
      {"  "}
      {error}
    </Message>
  ) : (
    <>
      <Container className="p-5">
        <Row>
          <Col>
            <div className="fix-image">
              <img
                src={product?.image}
                key={product?.id}
                className="text-center product-img img-fluid"
                alt={product?.title}
              />
              <br />
              {/* <button className='btn btn-link'>Wishlist</button> */}
              <div className="my-5 ">
                <Button onClick={handleAdd} className="addtocart">
                  <i className="bi bi-cart3"></i> Add to cart
                </Button>

                <Button
                  className="mx-4 
                      buynow"
                  onClick={handleBuyNow}
                >
                  <i className="bi bi-cash-coin"></i> Buy now{" "}
                </Button>
                <Button onClick={handleWishList} className="addtocart">
                  <i className="bi bi-heart"></i> Add to WishList
                </Button>
              </div>
            </div>
          </Col>

          <Col className="text-start">
            <h3 className="fs-3">{product?.title}</h3>
            <p className="fw-bolder" style={{ color: "green" }}>
              Special Price
            </p>
            <h4>
              &#8377;
              {Math.round(product?.price * 76)}
            </h4>
            <p>
              hurry up only{" "}
              <span className="fw-bold">{product?.rating?.count} </span> left{" "}
            </p>

            <h4>Avilable offers:</h4>
            <div className="my-4">
              <p>
                <i className="fa-solid fa-tag" style={{ color: "green" }}></i>{" "}
                <b>Bank Offer</b> 10% off on Citi Credit/Debit Cards, up to
                ₹1500. On orders of ₹5000 and above <a href="#!">T&C</a>
              </p>
              <p>
                <i className="fa-solid fa-tag" style={{ color: "green" }}></i>{" "}
                <b>Bank Offer</b> 5% Unlimited Cashback on Flipkart Axis Bank
                Credit Card
              </p>
              <p>
                <i className="fa-solid fa-tag" style={{ color: "green" }}></i>{" "}
                <b>Bank Offer</b> 10% off on Flipkart Paylater EMI Transactions
              </p>
            </div>
            <hr />

            <h4>About:</h4>
            <div className="my-4">
              <p className="text-capitalize">
                <span className="fw-bold">Category:</span> {product?.category}
              </p>
              <p>
                <span className="fw-bold">Description:</span>{" "}
                {product?.description}
              </p>
            </div>
            <hr />

            <h4>Ratings</h4>
            <p
              style={{ fontSize: "40px" }}
              className="d-flex align-items-center"
            >
              <span>{product?.rating?.rate} </span>
              <i
                className="bi bi-star-fill mx-2"
                style={{ color: "green", fontSize: "30px" }}
              ></i>
            </p>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Product_details;

/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { useState } from "react";
import { Col, Container, FloatingLabel, Form, Row } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { useLocation } from "react-router";
import Loading from "../../Components/Loading";
import Message from "../../Components/Message";
import ProductCard from "../../Components/ProductCard/ProductCard";
import {
  getProductsByCategory,
  clearFilter,
  getCategories,
} from "../../Redux/Products/ProductActions";
import NoItems from "../../Components/NoItems";

const DispProductList = () => {
  const { state } = useLocation();
  const categoryName = state?.catName;

  const productList = useSelector((state) => state.productList);
  const { loading, error, products, filter, filteredProducts } = productList;

  const categoryProducts = useSelector((state) => state.categoryProducts);
  const { loading: categoryLoading, error: categoryError } = productList;

  const categories = useSelector((state) => state.categoryList.categories);

  const [sorted, setSorted] = useState("Popularity");
  const [displayList, setDisplayList] = useState([]);
  const [filterArr, setFilterArr] = useState(
    categoryName ? [categoryName] : ["all"]
  );

  const dispatch = useDispatch();

  const sortArr = [
    "Popularity",
    "Ratings",
    "Price: High to Low",
    "Price: Low to High",
  ];

  // Sort Products
  const sortProducts = (sort) => {
    let arr = [...displayList];
    switch (sorted) {
      case "Popularity":
        arr.sort(function (a, b) {
          return b?.rating?.count - a?.rating?.count;
        });
        break;
      case "Ratings":
        arr.sort(function (a, b) {
          return b?.rating?.rate - a?.rating?.rate;
        });
        break;
      case "Price: Low to High":
        arr.sort(function (a, b) {
          return a?.price - b?.price;
        });
        break;
      case "Price: High to Low":
        arr.sort(function (a, b) {
          return b?.price - a?.price;
        });
        break;
      default:
        break;
    }
    setDisplayList(arr);
  };

  // Get products in category
  useEffect(() => {
    categoryName && dispatch(getProductsByCategory(categoryName));
  }, [categoryName]);

  // Set category products to display
  useEffect(() => {
    if (categoryProducts[categoryName])
      setDisplayList(categoryProducts[categoryName]);
  }, [categoryProducts]);

  // Set searched products
  useEffect(() => {
    if (filteredProducts) {
      if (sorted !== "Popularity") setSorted("Popularity");
      if (filterArr.length !== categories.length + 1) {
        setFilterArr(["all", ...categories]);
      }
      setDisplayList(filteredProducts);
    } else {
      setDisplayList(products);
    }
  }, [filteredProducts]);

  // Clear search when unmount component
  useEffect(() => {
    if (categories.length === 0) dispatch(getCategories());
    return () => {
      dispatch(clearFilter());
    };
  }, []);

  // Set Filtered products
  useEffect(() => {
    if (sorted !== "Popularity") setSorted("Popularity");
    if (filterArr.includes("all")) {
      setDisplayList(products);
    } else {
      let arr = products.filter((product) =>
        filterArr.includes(product.category)
      );
      setDisplayList(arr);
    }
  }, [filterArr, products]);

  // Get Sorted Products
  useEffect(() => {
    sortProducts(sorted);
  }, [sorted]);

  // useEffect(() => {
  //   if (filter && filterArr.length !== 1 && filterArr[0] !== "all") {
  //     setFilterArr(["all"]);
  //   }
  // }, [filter]);

  return (
    <Container fluid className="p-4">
      <Row className="ps-2">
        <Col md={2} className="p-3 bg-white disp-product">
          <h5>Filters</h5>
          <hr />

          {/* Sorting */}
          <FloatingLabel label="Sort By">
            <Form.Select
              value={sorted}
              onChange={(e) => setSorted(e.target.value)}
            >
              {sortArr.map((category, index) => (
                <option value={category} key={index}>
                  {category[0].toUpperCase()}
                  {category.slice(1)}
                </option>
              ))}
            </Form.Select>
          </FloatingLabel>
          <hr />

          {/* Filter by category */}
          <p className="fw-bold">Category</p>
          <Form.Group className="my-1">
            <Form.Check
              type="checkbox"
              label="All"
              value="all"
              onChange={(e) => {
                if (filteredProducts) dispatch(clearFilter());
                if (e.target.checked) {
                  setFilterArr(["all", ...categories]);
                } else {
                  setFilterArr(
                    filterArr.filter((item) => item !== e.target.value)
                  );
                }
              }}
              checked={filterArr.includes("all")}
            />
          </Form.Group>
          {categories.map((category, index) => (
            <Form.Group className="my-1" key={index}>
              <Form.Check
                type="checkbox"
                value={category}
                label={`${category[0].toUpperCase()}${category.slice(1)}`}
                onChange={(e) => {
                  if (filteredProducts) dispatch(clearFilter());
                  if (e.target.checked) {
                    // setFilterArr(filterArr.filter((item) => item !== "all"));
                    setFilterArr([...filterArr, e.target.value]);
                  } else {
                    setFilterArr(
                      filterArr
                        .filter((item) => item !== e.target.value)
                        .filter((item) => item !== "all")
                    );
                  }
                }}
                checked={filterArr.includes(category)}
              />
            </Form.Group>
          ))}
        </Col>
        <Col md={10}>
          {loading || categoryLoading ? (
            <Loading size={100} />
          ) : error || categoryError ? (
            <Message variant="danger">
              <i className="fa fa-warning text-danger" />
              {"  "}
              {error || categoryError}
            </Message>
          ) : displayList.length === 0 ? (
            <NoItems title="No Products Found." />
          ) : (
            <Container fluid className="pe-2">
              {filteredProducts && (
                <h3 className="mt-3">
                  Search Results{" "}
                  <small className="text-muted">("{filter}")</small>:
                </h3>
              )}
              <Row xs={1} md={4} className="justify-content-start disp-product">
                {displayList.map((product, index) => (
                  <ProductCard product={product} key={index} />
                ))}
              </Row>
            </Container>
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default DispProductList;

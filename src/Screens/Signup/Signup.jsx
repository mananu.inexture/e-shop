import React, { useEffect } from "react";
import UserForm from "../../Components/UserForm";
import signup_img from "../../Assets/Images/signup.svg";
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import Loading from "../../Components/Loading";
import Message from "../../Components/Message";
import { Col, Container, Row } from "react-bootstrap";

function Signup() {
  const userList = useSelector((state) => state.userList);
  const { loading, error, authUser } = userList;

  const navigate = useNavigate();

  useEffect(() => {
    if (authUser) {
      if (authUser.isAdmin) {
        navigate("/admin");
      } else {
        navigate("/");
      }
    }
    // eslint-disable-next-line
  }, [authUser]);

  return (
    <Container>
      {loading && <Loading />}
      {error && (
        <Message variant="danger">
          <i className="fa fa-warning text-danger" />
          {"  "}
          {error}
        </Message>
      )}
      <Row className="my-4 pt-5">
        <Col md={5} className="d-flex align-items-center">
          <img src={signup_img} className="img-fluid h-50 w-100" alt="signup" />
        </Col>
        <Col md={7}>
          <h3 className="text-center fw-bold mb-3">Sign Up</h3>
          <hr />
          <UserForm name="Sign Up" />
          <p className="text-center mt-1">
            Already Have an account ? <Link to="/login">Sign In</Link>
          </p>
        </Col>
      </Row>
    </Container>
  );
}

export default Signup;

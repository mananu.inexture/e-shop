import CryptoJS from "crypto-js";

// Encrypt object
export const encrypt = (object) => {
  const encryptedText = CryptoJS.AES.encrypt(
    JSON.stringify(object),
    "secret key"
  ).toString();
  return encryptedText;
};

// Decrypt token
export const decrypt = (str) => {
  try {
    const bytes = CryptoJS.AES.decrypt(str, "secret key");
    const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return decryptedData;
  } catch (err) {
    console.error(err.message);
  }
};

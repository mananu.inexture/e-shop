import * as yup from "yup";

// Signin Validation Schema
const loginSchema = yup
  .object({
    username: yup.string().required(),
    password: yup.string().required(),
  })
  .required();

export default loginSchema;

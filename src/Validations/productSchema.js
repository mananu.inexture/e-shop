import * as yup from "yup";

// Product Validation Schema
const productSchema = yup
  .object({
    title: yup
      .string()
      .matches(/^.{3,}$/, "Title can only contain alphabets.")
      .required("Title is required!")
      .required(),
    price: yup
      .string()
      .matches(/^[0-9-.,]*$/, "Price can only be numbers.")
      .required("Price is required!"),
    description: yup
      .string()
      .matches(/^.{30,}$/, "Product Description is too short.")
      .required(),
    category: yup.string().required(),
    image: yup
      .string()
      .matches(
        /^(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/,
        "Product Image url is invalid."
      )
      .required(),
  })
  .required();

export default productSchema;

import * as yup from "yup";

// Signup Validation Schema
const signupSchema = yup
  .object({
    firstname: yup
      .string()
      .required("Firstname is required!")
      .matches(/^([^0-9\W]*)$/, "Name can only contain alphabets."),
    lastname: yup
      .string()
      .required("Lastname is required!")
      .matches(/^([^0-9\W]*)$/, "Name can only contain alphabets."),
    email: yup.string().email().required(),
    phone: yup
      .string()
      .required("Phone is required!")
      .matches(/^[0-9]{10}$/, "Phone number must be 10 digits."),
    username: yup
      .string()
      .min(3, "Minimum 3 characters allowed")
      .required("Username is required!"),
    password: yup
      .string()
      .required("Password is required!")
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
      ),

    // number: yup
    //   .string()
    //   .matches(/^[0-9]*$/, "Block number can only be numbers.")
    //   .required("Block number is required!"),
    // street: yup.string().required("Street is required!"),
    city: yup
      .string()
      .matches(/^([^0-9\W]*)$/, "Name can only contain alphabets")
      .required("City is required!"),
    zipcode: yup
      .string()
      .matches(/^[0-9-]*$/, "Zipcode can only be numbers.")
      .required("Zipcode is required!"),
  })
  .required();

export default signupSchema;

import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import "./Assets/CSS/UserProfile.css";

ReactDOM.render(
  <React.StrictMode>
    <div className="d-flex flex-column flex-nowrap mw-100 overflow-hidden h-100">
      <App />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);